# Getting Started

Hi, and welcome

## Download GIT

* To get started with git, head to : [GIT SCM Page] (https://git-scm.com/download/win), download the version for your OS (i.e x64 or x32).
  * Mac or Linux users should already have git preinstalled.
* Install it.
* Open your **Git Bash** terminal.
* Configure your Git Credentials using the **git configure** coommand
  * Git global setup

    1. git config --global user.name "Your Gitlab Username"
    2. git config --global user.email "Your Email"

## Generating SSH Keys

You need SSH keys to push or pull code from the Gitlab repository.

To generate SSH Keys:

* Open your Git Bash terminal
* Type the following commands:
  * ssh-keygen -t rsa -C "your.email@example.com" -b 4096
* Copy your public key to the clipboard using the following command:
  * cat ~/.ssh/id_rsa.pub | clip
* Follow this [link] (https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html) to put copy your public key to gitlab.

## Commiting Your Code

### Create a new repository

* git clone http://10.91.91.110/telnet/repo_name.git
* cd timaas
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin master

### Existing folder

* cd existing_folder
* git init
* git remote add origin http://10.91.91.110/telnet/repo_name.git
* git add .
* git commit -m "Initial commit"
* git push -u origin master

### Existing Git repository

* cd existing_repo
* git remote rename origin old-origin
* git remote add origin http://10.91.91.110/telnet/repo_name.git
* git push -u origin --all
* git push -u origin --tags
