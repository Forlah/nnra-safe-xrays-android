package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 3/20/17.
 */
public class Questions {

  String id;
  String Question_Text;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getQuestion_Text() {
    return Question_Text;
  }

  public void setQuestion_Text(String question_Text) {
    Question_Text = question_Text;
  }
}
