package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 2/20/17.
 */
public class HospitalService {

  String Id;
  String ServiceTitle;

  public String getId() {
    return Id;
  }

  public void setId(String id) {
    Id = id;
  }

  public String getServiceTitle() {
    return ServiceTitle;
  }

  public void setServiceTitle(String serviceTitle) {
    ServiceTitle = serviceTitle;
  }
}
