package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 3/2/17.
 */

import java.util.ArrayList;

/**
 * Created by FOLASHELE on 10/25/2015.
 */
public class Groups {

  private String Question;
  private ArrayList<String> Answer;
  private String mAnswer;

  public String getmAnswer() {
    return mAnswer;
  }

  public void setmAnswer(String mAnswer) {
    this.mAnswer = mAnswer;
  }

  public String getQuestion() {
    return Question;
  }

  public void setQuestion(String question) {
    Question = question;
  }

  public ArrayList<String> getAnswer() {
    return Answer;
  }

  public void setAnswer(ArrayList<String> answer) {
    Answer = answer;
  }
}

