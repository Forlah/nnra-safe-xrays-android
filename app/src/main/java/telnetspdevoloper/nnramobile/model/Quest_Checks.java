package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 3/21/17.
 */
public class Quest_Checks {

  String YesCheckBox_Value;
  String NoCheckBox_Value;
  int answer;
  int QuestionId;

  public int getAnswer() {
    return answer;
  }

  public void setAnswer(int answer) {
    this.answer = answer;
  }

  public int getQuestionId() {
    return QuestionId;
  }

  public void setQuestionId(int questionId) {
    QuestionId = questionId;
  }

  public String getYesCheckBox_Value() {
    return YesCheckBox_Value;
  }

  public void setYesCheckBox_Value(String yesCheckBox_Value) {
    YesCheckBox_Value = yesCheckBox_Value;
  }

  public String getNoCheckBox_Value() {
    return NoCheckBox_Value;
  }

  public void setNoCheckBox_Value(String noCheckBox_Value) {
    NoCheckBox_Value = noCheckBox_Value;
  }
}
