package telnetspdevoloper.nnramobile.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sp_developer on 2/21/17.
 */
public class Booking {  // pojo for converting to required json object for booking an appointment with a medical center

  String customerName;
  String email;
  String appointmentDate;
  String description;
  String phone;
  Hospital medicalCenter = new Hospital();
  ;
  List<HospitalService> Services = new ArrayList<>();

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAppointmentDate() {
    return appointmentDate;
  }

  public void setAppointmentDate(String appointmentDate) {
    this.appointmentDate = appointmentDate;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Hospital getMedicalCenter() {

    return medicalCenter;
  }

  public void setMedicalCenter(Hospital medicalCenter) {
    this.medicalCenter = medicalCenter;
  }

  public List<HospitalService> getServices() {
    new HospitalService();
    return Services;
  }

  public void setServices(List<HospitalService> services) {
    Services = services;
  }

}
