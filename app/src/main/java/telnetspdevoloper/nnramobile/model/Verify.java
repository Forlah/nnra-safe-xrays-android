package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 4/27/17.
 */
public class Verify {

  private String centerName;
  private String centerAddress;
  private String status;

  public String getCenterName() {
    return centerName;
  }

  public void setCenterName(String centerName) {
    this.centerName = centerName;
  }

  public String getCenterAddress() {
    return centerAddress;
  }

  public void setCenterAddress(String centerAddress) {
    this.centerAddress = centerAddress;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
