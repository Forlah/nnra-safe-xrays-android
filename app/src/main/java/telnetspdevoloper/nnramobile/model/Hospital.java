package telnetspdevoloper.nnramobile.model;

/**
 * Created by sp_developer on 2/16/17.
 */
public class Hospital {

  String id;
  int CenterId;
  String HospitalName;
  String HospitalAddress;
  String HospitialPhone;
  String HospitalEmail;
  String HospitalCity;
  String HospitalState;
  String HospitalLatitude;
  String HospitalLongitude;
  String HospitalServices;
  String HospitalfullAddress;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getCenterId() {
    return CenterId;
  }

  public void setCenterId(int centerId) {
    CenterId = centerId;
  }

  public String getHospitalName() {
    return HospitalName;
  }

  public void setHospitalName(String hospitalName) {
    HospitalName = hospitalName;
  }

  public String getHospitalAddress() {
    return HospitalAddress;
  }

  public void setHospitalAddress(String hospitalAddress) {
    HospitalAddress = hospitalAddress;
  }

  public String getHospitialPhone() {
    return HospitialPhone;
  }

  public void setHospitialPhone(String hospitialPhone) {
    HospitialPhone = hospitialPhone;
  }

  public String getHospitalEmail() {
    return HospitalEmail;
  }

  public void setHospitalEmail(String hospitalEmail) {
    HospitalEmail = hospitalEmail;
  }

  public String getHospitalCity() {
    return HospitalCity;
  }

  public void setHospitalCity(String hospitalCity) {
    HospitalCity = hospitalCity;
  }

  public String getHospitalState() {
    return HospitalState;
  }

  public void setHospitalState(String hospitalState) {
    HospitalState = hospitalState;
  }

  public String getHospitalLatitude() {
    return HospitalLatitude;
  }

  public void setHospitalLatitude(String hospitalLatitude) {
    HospitalLatitude = hospitalLatitude;
  }

  public String getHospitalLongitude() {
    return HospitalLongitude;
  }

  public void setHospitalLongitude(String hospitalLongitude) {
    HospitalLongitude = hospitalLongitude;
  }

  public String getHospitalServices() {
    return HospitalServices;
  }

  public void setHospitalServices(String hospitalServices) {
    HospitalServices = hospitalServices;
  }

  public String getHospitalfullAddress() {
    return HospitalfullAddress;
  }

  public void setHospitalfullAddress(String hospitalfullAddress) {
    HospitalfullAddress = hospitalfullAddress;
  }
}
