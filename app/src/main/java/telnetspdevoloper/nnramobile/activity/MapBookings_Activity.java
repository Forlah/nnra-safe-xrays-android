package telnetspdevoloper.nnramobile.activity;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.Application.ConnectionChecker;
import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.ConnectivityReceiver;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.Utilities.SimpleDividerItemDecoration;
import telnetspdevoloper.nnramobile.adapters.Booking_RecyclerAdapter;
import telnetspdevoloper.nnramobile.adapters.MapSearch_RecyclerAdapter;
import telnetspdevoloper.nnramobile.model.Hospital;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;
import telnetspdevoloper.nnramobile.service.Volley_StringRequest;

public class MapBookings_Activity extends AppCompatActivity implements OnMapReadyCallback,
    LocationListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    ConnectivityReceiver.ConnectivityReceiverListener {

  private GoogleMap myMap;
  GoogleApiClient mGoogleApiClient;
  Boolean mapLoaded = false;

  private static final String MYTAG = "MYTAG";
  private String[] strArrData = {"No Suggestions"};

  SearchView searchView = null;
  Boolean checkGPS = false;
  Boolean checkNetwork = false;
  public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;

  LocationRequest mLocationRequest;
  ProgressBar progress;

  LatLng latLng;
  Marker currLocationMarker;
  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class

  private ArrayList<Hospital> Hospital_List;

  RecyclerView mRecycler;
  LinearLayoutManager mLayoutManager = null;
  Booking_RecyclerAdapter adapter;
  private SimpleCursorAdapter myAdapter;
  private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete";
  private static final String OUT_TYPE = "/json";
  private static final String API_KEY = "AIzaSyDBazIiBn2tTmqcSpkH65Xq5doTSuOo22A";
  private ConnChecker connChecker = new ConnChecker(MapBookings_Activity.this);

  private void autocomplete(String search_text) {

    final ArrayList<String> resultList = new ArrayList<>();

    StringBuilder sb = new StringBuilder(PLACES_API_BASE + OUT_TYPE);
    sb.append("?key=" + API_KEY);
    sb.append("&components=country:ng");
    sb.append("&input=" + search_text.replaceAll(" ", "%20"));

    Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.GET, sb.toString(),
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            try {
              // create a JSON object hierarchy from the results
              JSONObject jsonObj = new JSONObject(response);
              System.out.println("The response from google = " + jsonObj.toString());
              JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

              // extract the Place descriptions from the results
              for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).get("description").toString());
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }

            strArrData = resultList.toArray(new String[resultList.size()]);

          }
        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {

      }
    });

    stringRequest.setPriority(Request.Priority.HIGH);
    helper.add(stringRequest);

  }

  private void ShowMarkers(double mLatitude, double mLongitude) {

    String url = EndPoints.BASE_2 + "NNRA/getListOfCordinates/" + mLatitude + "/" + mLongitude;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                Hospital_List = new ArrayList<>();

                for (int i = 0; i < response.length(); i++) {

                  try {

                    String id = response.getJSONObject(i).get("facilityID").toString();
                    int centerId = response.getJSONObject(i).getInt("id");
                    String name = response.getJSONObject(i).get("centerName").toString();
                    String address = response.getJSONObject(i).get("address").toString();
                    String longitude = response.getJSONObject(i).get("longitude").toString();
                    String latitude = response.getJSONObject(i).get("latitude").toString();
                    String email = response.getJSONObject(i).get("email").toString();

                    Hospital hospital = new Hospital();
                    hospital.setId(id);
                    hospital.setCenterId(centerId);
                    hospital.setHospitalName(name);
                    hospital.setHospitalAddress(address);
                    hospital.setHospitalLatitude(latitude);
                    hospital.setHospitalLongitude(longitude);
                    hospital.setHospitalEmail(email);

                    Hospital_List.add(0, hospital);

                  } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                        Toast.LENGTH_LONG).show();
                  }

                }

                // plot center(s) on google map
                for (int i = 0; i < Hospital_List.size(); i++) {

                  // Add Marker to Map
                  MarkerOptions options = new MarkerOptions();
                  options.title(Hospital_List.get(i).getHospitalName());
                  options.snippet(Hospital_List.get(i).getHospitalAddress());
                  //options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
                  options.position(
                      new LatLng(Double.parseDouble(Hospital_List.get(i).getHospitalLatitude()),
                          Double.parseDouble(Hospital_List.get(i).getHospitalLongitude())));
                  Marker currentMarker = myMap.addMarker(options);
                  currentMarker.showInfoWindow();

                }

                progress.setVisibility(View.GONE); // dismiss progress dialog
                // setup recycler view and its adapter
                adapter = new Booking_RecyclerAdapter(MapBookings_Activity.this, Hospital_List,
                    new Booking_RecyclerAdapter.bookOnClickListener() {
                      @Override
                      public void onClick(int position) {

                        Intent i = new Intent(MapBookings_Activity.this, Book_Activity.class);
                        i.putExtra("KEY_CENTER_ID", Hospital_List.get(position).getId());
                        i.putExtra("KEY_CENTER_Id", Hospital_List.get(position).getCenterId());
                        i.putExtra("Key_Center_Name",
                            Hospital_List.get(position).getHospitalName());
                        i.putExtra("Key_Center_Address",
                            Hospital_List.get(position).getHospitalAddress());
                        i.putExtra("Key_Center_email",
                            Hospital_List.get(position).getHospitalEmail());
                        startActivity(i);
                      }
                    });
                mRecycler.setAdapter(adapter);

              } else { // nothing found

                myMap.clear();

                progress.setVisibility(View.GONE); // dismiss progress dialog
                adapter = null;
                mRecycler.setAdapter(adapter);
                Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                    Toast.LENGTH_LONG).show();

              }

            } catch (Exception e) {
              e.printStackTrace();
              Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private void Show_Markers(double mLatitude,
      double mLongitude) { // this method is for showing markers after a search has been done in order for map camera to navigate to the position

    String url = EndPoints.BASE_2 + "NNRA/getListOfCordinates/" + mLatitude + "/" + mLongitude;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                Hospital_List = new ArrayList<>();
                //mapMessage.setVisibility(View.GONE); // hide no centers available message

                myMap.clear();

                for (int i = 0; i < response.length(); i++) {

                  try {

                    String id = response.getJSONObject(i).get("facilityID").toString();
                    int centerId = response.getJSONObject(i).getInt("id");
                    String name = response.getJSONObject(i).get("centerName").toString();
                    String address = response.getJSONObject(i).get("address").toString();
                    String longitude = response.getJSONObject(i).get("longitude").toString();
                    String latitude = response.getJSONObject(i).get("latitude").toString();
                    String email = response.getJSONObject(i).get("email").toString();

                    Hospital hospital = new Hospital();
                    hospital.setId(id);
                    hospital.setCenterId(centerId);
                    hospital.setHospitalName(name);
                    hospital.setHospitalAddress(address);
                    hospital.setHospitalLatitude(latitude);
                    hospital.setHospitalLongitude(longitude);
                    hospital.setHospitalEmail(email);

                    Hospital_List.add(0, hospital);

                  } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                        Toast.LENGTH_LONG).show();

                  }

                }

                // plot center(s) on google map
                for (int i = 0; i < Hospital_List.size(); i++) {

                  // Add Marker to Map
                  MarkerOptions options = new MarkerOptions();
                  options.title(Hospital_List.get(i).getHospitalName());
                  options.snippet(Hospital_List.get(i).getHospitalAddress());
                  //options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
                  options.position(
                      new LatLng(Double.parseDouble(Hospital_List.get(i).getHospitalLatitude()),
                          Double.parseDouble(Hospital_List.get(i).getHospitalLongitude())));
                  Marker currentMarker = myMap.addMarker(options);
                  currentMarker.showInfoWindow();

                }

                //zoom to current position:
                LatLng newPosition = new LatLng(
                    Double.parseDouble(Hospital_List.get(0).getHospitalLatitude()),
                    Double.parseDouble(Hospital_List.get(0).getHospitalLongitude()));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(newPosition)
                    .tilt(40).zoom(15).build();

                myMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

                progress.setVisibility(View.GONE); // dismiss progress dialog
                // setup recycler view and its adapter
                adapter = new Booking_RecyclerAdapter(MapBookings_Activity.this, Hospital_List,
                    new Booking_RecyclerAdapter.bookOnClickListener() {
                      @Override
                      public void onClick(int position) {

                        Intent i = new Intent(MapBookings_Activity.this, Book_Activity.class);
                        i.putExtra("KEY_CENTER_ID", Hospital_List.get(position).getId());
                        i.putExtra("KEY_CENTER_Id", Hospital_List.get(position).getCenterId());
                        i.putExtra("Key_Center_Name",
                            Hospital_List.get(position).getHospitalName());
                        i.putExtra("Key_Center_Address",
                            Hospital_List.get(position).getHospitalAddress());
                        i.putExtra("Key_Center_email",
                            Hospital_List.get(position).getHospitalEmail());
                        startActivity(i);

                      }
                    });
                mRecycler.setAdapter(adapter);

              } else { // nothing found

                myMap.clear();

                progress.setVisibility(View.GONE); // dismiss progress dialog
                adapter = null;
                mRecycler.setAdapter(adapter);
                //mapMessage.setVisibility(View.VISIBLE);
                Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                    Toast.LENGTH_LONG).show();

              }

            } catch (Exception e) {
              e.printStackTrace();
              Toast.makeText(MapBookings_Activity.this, "Center(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  public void showSettingsAlert() {
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapBookings_Activity.this);

    alertDialog.setTitle("GPS Not Enabled");

    alertDialog.setMessage("Do you want to turn On GPS");

    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        MapBookings_Activity.this.startActivity(intent);
      }
    });

    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.cancel();
      }
    });

    alertDialog.show();
  }

  // Showing the status in Snackbar
  private void showSnack(boolean isConnected) {
    String message = "";
    int color = -0;
//        if (isConnected) {
//           // message = "Good! Connected to Internet";
//            //color = Color.WHITE;
//        } else {
//            message = "Sorry! No Connection To Internet";
//        }

    if (!isConnected) {
      message = "Sorry! No Connection To Internet";

      Snackbar snackbar = Snackbar
          .make(findViewById(R.id.list_hospital_centers), message, Snackbar.LENGTH_LONG);

      View sbView = snackbar.getView();
      //sbView.setBackgroundColor();
      TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
      textView.setTextColor(ContextCompat.getColor(MapBookings_Activity.this, R.color.White));
      snackbar.show();
    }

  }

  private void askPermissionsAndShowMyLocation() {

    // with API >= 23, you have to ask the user for permission to view their location.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      int accessCoarsePermission = ContextCompat.checkSelfPermission(MapBookings_Activity.this,
          android.Manifest.permission.ACCESS_COARSE_LOCATION);
      int accessFinePermission = ContextCompat.checkSelfPermission(MapBookings_Activity.this,
          android.Manifest.permission.ACCESS_FINE_LOCATION);

      if (accessCoarsePermission != PackageManager.PERMISSION_GRANTED ||
          accessFinePermission != PackageManager.PERMISSION_GRANTED) {

        // The permission to ask user
        String[] permissions = new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION};

        // show a dialog asking the user to allow the above permissions.
        ActivityCompat.requestPermissions(MapBookings_Activity.this, permissions,
            REQUEST_ID_ACCESS_COURSE_FINE_LOCATION);

        return;
      }
    }

    // Show current location on Map
    myMap.setMyLocationEnabled(true);
    //showMarkers();
  }

  protected synchronized void buildGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
  }

  private void onMyMapReady(GoogleMap googleMap) {
    // Get Google Map from Fragment
    myMap = googleMap;
    // Set onMapLoadedCallback listner
    myMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
      @Override
      public void onMapLoaded() {
        // Map loaded, Dismiss the dialog

        mapLoaded = true;

        askPermissionsAndShowMyLocation();

        if (ActivityCompat.checkSelfPermission(MapBookings_Activity.this,
            android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(MapBookings_Activity.this,
            android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        if (checkGPS) {

          myMap.setMyLocationEnabled(true);
          buildGoogleApiClient();

          mGoogleApiClient.connect();

        } else {

          showSettingsAlert();
        }

      }
    });
    myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    myMap.getUiSettings().setZoomControlsEnabled(true);

  }

  private void getLocation(String url) {

    progress.setVisibility(View.VISIBLE); // show progress bar

    Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.GET, url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            JSONObject location;
            Double lng = 0.0;
            Double lat = 0.0;

            try {

              location = new JSONObject(response);

              lng = ((JSONArray) location.get("results")).getJSONObject(0).getJSONObject("geometry")
                  .getJSONObject("location")
                  .getDouble("lng");

              lat = ((JSONArray) location.get("results")).getJSONObject(0).getJSONObject("geometry")
                  .getJSONObject("location")
                  .getDouble("lat");

            } catch (JSONException e) {
              e.printStackTrace();
            }

            Show_Markers(lat, lng);

          }
        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {

        Toast.makeText(MapBookings_Activity.this, "Location not found", Toast.LENGTH_LONG).show();
      }
    });

    stringRequest.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    stringRequest.setRetryPolicy(policy);
    helper.add(stringRequest);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bookings_);

    mRecycler = (RecyclerView) findViewById(R.id.list_hospital_booking_centers);
    mRecycler.setHasFixedSize(true);
    mRecycler.setItemAnimator(new DefaultItemAnimator());
    mRecycler.addItemDecoration(new SimpleDividerItemDecoration(MapBookings_Activity.this));
    mLayoutManager = new LinearLayoutManager(MapBookings_Activity.this);
    mRecycler.setLayoutManager(mLayoutManager);

    progress = (ProgressBar) findViewById(R.id.map_progress);

    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.booking_map_fragment);

    // set callback listner, on Google Map ready
    mapFragment.getMapAsync(new OnMapReadyCallback() {
      @Override
      public void onMapReady(GoogleMap googleMap) {
        onMyMapReady(googleMap);

        // getting GPS status
        LocationManager locationManager = (LocationManager) MapBookings_Activity.this
            .getSystemService(LOCATION_SERVICE);

        checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

      }
    });

//        Hospital_List = new ArrayList<>();
//        for (int i = 0; i < 15; i++) {
//            Hospital obj = new Hospital();
//            obj.setHospitalName("NNRA Approved Hospital/Center Name "+i);
//            obj.setHospitalAddress("Hospital/Center Address "+i);
//
//            Hospital_List.add(0, obj);
//        }

//        adapter = new Booking_RecyclerAdapter(MapBookings_Activity.this, Hospital_List, new Booking_RecyclerAdapter.bookOnClickListener() {
//            @Override
//            public void onClick(int position) {
//
//                Intent i = new Intent(MapBookings_Activity.this, Book_Activity.class);
//                i.putExtra("KEY_CENTER_ID", Hospital_List.get(position).getId());
//                i.putExtra("Key_Center_Name", Hospital_List.get(position).getHospitalName());
//                startActivity(i);
//            }
//        });
//        mRecycler.setAdapter(adapter);

    final String[] from = new String[]{"Result"};
    final int[] to = new int[]{android.R.id.text1};

    // setup SimpleCursorAdapter
    myAdapter = new SimpleCursorAdapter(MapBookings_Activity.this,
        android.R.layout.simple_spinner_dropdown_item, null,
        from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    switch (id) {
      case android.R.id.home:
        MapBookings_Activity.this.finish();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onStart() {
    super.onStart();

    if (mapLoaded) {

      // getting GPS status
      LocationManager locationManager = (LocationManager) MapBookings_Activity.this
          .getSystemService(LOCATION_SERVICE);

      checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

      if (checkGPS) {
        if (ActivityCompat
            .checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat
            .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        myMap.setMyLocationEnabled(true);
        buildGoogleApiClient();

        mGoogleApiClient.connect();
      } else {

        showSettingsAlert();

      }
    }

  }

  // When you have the request results
  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    switch (requestCode) {
      case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {
        // NOte: If request is cancelled, the result arrays are empty.
        // Permissions granted (read/write)
        if (grantResults.length > 1
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

          Toast.makeText(MapBookings_Activity.this, "Permission granted!", Toast.LENGTH_LONG)
              .show();

        }
        // Cancelled or denied.
        else {
          Toast.makeText(MapBookings_Activity.this, "Permission Denied!", Toast.LENGTH_LONG).show();
        }

        break;
      }
    }
  }

  // Find Location provider is openning
  private String getEnabledLoactionProvider() {

    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    // Criteria to find location provider
    Criteria criteria = new Criteria();

    // Returns the name of the provider that best meets the given criteria.
    // ==> "gps", "network",... etc
    String bestProvider = locationManager.getBestProvider(criteria, true);

    boolean enabled = locationManager.isProviderEnabled(bestProvider);

    if (!enabled) {
      Toast.makeText(MapBookings_Activity.this, "No Location provider enabled!", Toast.LENGTH_LONG)
          .show();
      return null;
    }

    return bestProvider;
  }

  @Override
  public void onLocationChanged(Location location) {

    //place marker at current position
    //mGoogleMap.clear();
    if (currLocationMarker != null) {
      currLocationMarker.remove();
      currLocationMarker = null;

    }
    latLng = new LatLng(location.getLatitude(), location.getLongitude());
    MarkerOptions markerOptions = new MarkerOptions();
    markerOptions.position(latLng);
    markerOptions.title("Your Location");
    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
    currLocationMarker = myMap.addMarker(markerOptions);

    //zoom to current position:
    CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(latLng)
        .tilt(40).zoom(15).build();

    myMap.animateCamera(CameraUpdateFactory
        .newCameraPosition(cameraPosition));

    //If you only need one location, unregister the listener
    //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    // new getLocations().execute();  // get coordinate location from service

  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    ActivityCompat#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for ActivityCompat#requestPermissions for more details.
      return;
    }
    try {

      LocationManager locationManager = (LocationManager) MapBookings_Activity.this
          .getSystemService(LOCATION_SERVICE);

      // getting GPS status
      checkGPS = locationManager
          .isProviderEnabled(LocationManager.GPS_PROVIDER);

      if (!checkGPS) {

        showSettingsAlert();
      } else {

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
            mGoogleApiClient);
        if (mLastLocation != null) {

          if (currLocationMarker != null) {
            currLocationMarker.remove();
            currLocationMarker = null;

          }

          latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
          MarkerOptions markerOptions = new MarkerOptions();
          markerOptions.position(latLng);
          markerOptions.title("Your Location");
          markerOptions
              .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
          currLocationMarker = myMap.addMarker(markerOptions);

          if (connChecker.isAvailable()) {

            // get coordinate location from service and plot pushpin on google map
            ShowMarkers(mLastLocation.getLatitude(), mLastLocation.getLongitude());

          } else {

            Toast.makeText(MapBookings_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }

        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(200000); // milliseconds
        //mLocationRequest.setFastestInterval(15000); // milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi
            .requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapBookings_Activity.this);

      }
    } catch (Exception e) {

    }

  }

  @Override
  protected void onResume() {
    super.onResume();

    // register connection status listener
    ConnectionChecker.getInstance().setConnectivityListener(this);
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void onMapReady(GoogleMap googleMap) {

  }

  @Override
  public void onNetworkConnectionChanged(boolean isConnected) {
    showSnack(isConnected);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    // add item to action bar
    getMenuInflater().inflate(R.menu.menu_search, menu);

    // Get Search item from action bar and Get Search Service
    MenuItem searchItem = menu.findItem(R.id.search);
    SearchManager searchManager = (SearchManager) MapBookings_Activity.this
        .getSystemService(Context.SEARCH_SERVICE);
    if (searchItem != null) {

      searchView = (SearchView) searchItem.getActionView();
    }

    if (searchView != null) {
      searchView.setSearchableInfo(
          searchManager.getSearchableInfo(MapBookings_Activity.this.getComponentName()));
      searchView.setIconified(true);
      searchView.setSuggestionsAdapter(myAdapter);

      // Getting selected(clicked) items suggested
      searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
        @Override
        public boolean onSuggestionSelect(int position) {
          return true;
        }

        @Override
        public boolean onSuggestionClick(int position) {
          // Add clicked text to search box
          CursorAdapter cursorAdapter = searchView.getSuggestionsAdapter();
          Cursor cursor = cursorAdapter.getCursor();
          cursor.moveToPosition(position);
          searchView.setQuery(cursor.getString(cursor.getColumnIndex("Result")), false);

          // Dismiss keyboard
          InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
          imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

          String address = cursor.getString(cursor.getColumnIndex("Result")).trim();
          if (!address.isEmpty()) {
            // get coordinate of selected location
            String API_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address
                .replaceAll(" ", "%20") + "&key=" + API_KEY + "&sensor=false";

            getLocation(API_URL); // get coordinate of address from google places API

          }

          return true;
        }
      });

      searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

        @Override
        public boolean onQueryTextSubmit(String query) {
          return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {

          autocomplete(newText.trim());

          System.out.println("String array data size = " + strArrData.length);
          // Filter data
          final MatrixCursor mc = new MatrixCursor(new String[]{BaseColumns._ID, "Result"});
          for (int i = 0; i < strArrData.length; i++) {

            if (strArrData[i].toLowerCase().startsWith(newText.toLowerCase())) {
              mc.addRow(new Object[]{i, strArrData[i]});
            }

          }
          myAdapter.changeCursor(mc);
          return false;
        }
      });
    }

    return true;

  }

}
