package telnetspdevoloper.nnramobile.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.Alert_Dialog;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.Utilities.Progressdialog;
import telnetspdevoloper.nnramobile.adapters.Booking_RecyclerAdapter;
import telnetspdevoloper.nnramobile.adapters.Questionnaire_ListAdaper;
import telnetspdevoloper.nnramobile.adapters.Questionnaire_RecyclerAdapter;
import telnetspdevoloper.nnramobile.model.Booking;
import telnetspdevoloper.nnramobile.model.Hospital;
import telnetspdevoloper.nnramobile.model.Quest_Checks;
import telnetspdevoloper.nnramobile.model.Questions;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;
import telnetspdevoloper.nnramobile.service.Volley_StringRequest;

public class FeedBack_Activity extends AppCompatActivity {

  private EditText mRefId, mComment;
  private RatingBar mRating;

  private Button SendBtn;
  private String RefId, Comment;
  private int Rating;
  private ArrayList<Questions> List_items = new ArrayList<>();
  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class
  private CoordinatorLayout coordinatorLayout;

  private ConnChecker connChecker = new ConnChecker(FeedBack_Activity.this);

  public Questionnaire_RecyclerAdapter adapter;
  private RecyclerView mRecyclerView;
  LinearLayoutManager mLayoutManager = null;

  private JSONObject toJSON(JSONArray array) {  // convert to json

    JSONObject jsonObject = new JSONObject();
    JSONObject jsonObject_medical = new JSONObject();

    try {
      jsonObject.put("description", Comment);
      jsonObject.put("ratings", Rating);
      jsonObject.put("booking", jsonObject_medical.put("id", RefId));
      jsonObject.put("questionnaire", array);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return jsonObject;
  }

  private ArrayList<Quest_Checks> getQuestionnaireResult() {

    ArrayList<Quest_Checks> questions_results = new ArrayList<>();

    for (int i = 0; i < List_items.size(); i++) {

      String Yes_value = "0";
      String NO_value = "0";
      int ans = 0;

      Quest_Checks obj = new Quest_Checks();

      //System.out.println("Position "+i+" Yes checkbox = "+adapter.getmCheckStates_Yes().get(i)+" No Checkbox = "+adapter.getmCheckStates_No().get(i));

      if (adapter.getmCheckStates_Yes().get(i) == true) {

        Yes_value = "1";
        ans = 1;
      } else if (adapter.getmCheckStates_No().get(i) == true) {

        NO_value = "1";
        ans = 0;
      }

      obj.setQuestionId(Integer.parseInt(List_items.get(i).getId()));
      obj.setAnswer(ans);
      obj.setNoCheckBox_Value(NO_value);
      obj.setYesCheckBox_Value(Yes_value);

      questions_results.add(obj);

    }

    return questions_results;
  }

  private void getQuestionnaires() {

    String url = EndPoints.BASE + "getQuestionaire";
    coordinatorLayout.setVisibility(View.GONE);
    final Progressdialog pd = new Progressdialog();
    pd.show_progress_no_title(FeedBack_Activity.this, "Please Wait..");

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                List_items = new ArrayList<>();

                for (int i = 0; i < response.length(); i++) {

                  try {

                    String id = response.getJSONObject(i).get("id").toString();
                    String question = response.getJSONObject(i).get("question").toString();

                    Questions questions = new Questions();
                    questions.setId(id);
                    questions.setQuestion_Text(question);

                    List_items.add(0, questions);

                  } catch (JSONException e) {
                    e.printStackTrace();

                    //   Toast.makeText(FeedBack_Activity.this, "Center(s) Currently Not Available", Toast.LENGTH_LONG).show();
                  }

                }

                // setup recycler view and its adapter

                coordinatorLayout.setVisibility(View.VISIBLE);

                pd.dismissProgress();
                adapter = new Questionnaire_RecyclerAdapter(FeedBack_Activity.this, List_items);
                mRecyclerView.setAdapter(adapter);

              } else { // nothing found

                pd.dismissProgress();

              }

            } catch (Exception e) {
              e.printStackTrace();
              pd.dismissProgress();

              Toast.makeText(FeedBack_Activity.this, "Connection Timeout..Try again",
                  Toast.LENGTH_LONG).show();
            }
          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        pd.dismissProgress();

        new Alert_Dialog(FeedBack_Activity.this, "Connection Lost",
            "Please Ensure You Have Internet Connection.");
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private void submitFeedBackTask() {

    final Progressdialog pd = new Progressdialog();
    pd.show_progress_no_title(FeedBack_Activity.this, "Submitting FeedBack..");

    final String requestBody = toJSON(QuestionnaireResult_toJSON(getQuestionnaireResult()))
        .toString();

    String url = EndPoints.BASE + "logFeedBack";

    final Volley_StringRequest request = new Volley_StringRequest(Request.Method.POST, url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            pd.dismissProgress();

            if (response.startsWith("1")) {

              Snackbar snackbar = Snackbar
                  .make(coordinatorLayout, "FeedBack Sent !", Snackbar.LENGTH_LONG);
              View snackbarLayout = snackbar.getView();
              TextView textView = (TextView) snackbarLayout
                  .findViewById(android.support.design.R.id.snackbar_text);
              textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.checked, 0);
              // textView.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.cardview_default_radius));

              snackbar.show();

              Thread timerThread = new Thread() {

                public void run() {

                  try {
                    sleep(2400);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  } finally {

                    FeedBack_Activity.this.finish();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                  }
                }

              };

              timerThread.start();

            } else if (response.startsWith("0")) {

              final Snackbar snackBar = Snackbar.make(coordinatorLayout,
                  "Unable To Submit Your Feedback. Please Check Your Reference Number And Try Again",
                  Snackbar.LENGTH_INDEFINITE);

              snackBar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  snackBar.dismiss();
                }
              });

              snackBar.show();
            } else {

              new Alert_Dialog(FeedBack_Activity.this, response.toString());
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        Toast.makeText(FeedBack_Activity.this, "Failed To Establish Connection To Remote Server",
            Toast.LENGTH_LONG).show();
        pd.dismissProgress();
      }

    }) {
      @Override
      public String getBodyContentType() {
        return "application/json; charset=utf-8";
      }

      @Override
      public byte[] getBody() throws AuthFailureError {

        try {

          return requestBody == null ? null : requestBody.getBytes("utf-8");

        } catch (UnsupportedEncodingException uee) {
          VolleyLog
              .wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody,
                  "utf-8");
          return null;
        }
      }

      @Override
      protected Response<String> parseNetworkResponse(
          NetworkResponse response) { // can get response code here
        return super.parseNetworkResponse(response);
      }

    };

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private JSONArray QuestionnaireResult_toJSON(ArrayList<Quest_Checks> values) {

    JSONArray jsonArray;
    JSONObject jsonObject;

    try {

      jsonArray = new JSONArray();

      for (int i = 0; i < values.size(); i++) {

        jsonObject = new JSONObject();

        jsonObject.put("id", values.get(i).getQuestionId());
        jsonObject.put("answer", values.get(i).getAnswer());

        jsonArray.put(jsonObject);

      }

      return jsonArray;

    } catch (Exception e) {
      e.printStackTrace();

      jsonArray = null;
    }

    return jsonArray;
  }

  private Boolean validateQuestionnaires(ArrayList<Quest_Checks> QuestResult) {
    Boolean isComplete = false;

    for (int i = 0; i < QuestResult.size(); i++) {

      if (QuestResult.get(i).getNoCheckBox_Value().equals("0") && QuestResult.get(i)
          .getYesCheckBox_Value().equals("0")) {
        isComplete = false;
        break;
      } else {

        isComplete = true;

      }
    }

    return isComplete;

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_feed_back);

    coordinatorLayout = (CoordinatorLayout) findViewById(R.id
        .coordinatorlayout);
    mRefId = (EditText) findViewById(R.id.feedback_ref_number);
    mComment = (EditText) findViewById(R.id.feedback_comment);
    mRating = (RatingBar) findViewById(R.id.ratingBar);
    SendBtn = (Button) findViewById(R.id.post_btn);

    mRecyclerView = (RecyclerView) findViewById(R.id.questionnaire_recycler);

    if (connChecker.isAvailable()) {
      getQuestionnaires();
    } else {

      Toast.makeText(FeedBack_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
          .show();
    }

    mRecyclerView.setHasFixedSize(true);

    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(FeedBack_Activity.this);
    mRecyclerView.setLayoutManager(mLayoutManager);

    // disable send button by default
    SendBtn.setEnabled(false);
    SendBtn.setAlpha(0.5f);

//        for (int i = 0; i < 13; i++){
//            Questions questions = new Questions();
//            questions.setId(String.valueOf(i));
//            questions.setQuestion_Text("Are there adequate control over entries into supervised areas and appropriate postings?");
//
//            List_items.add(0, questions);
//
//        }
//
//        adapter =  new Questionnaire_RecyclerAdapter(FeedBack_Activity.this, List_items);
//        mRecyclerView.setAdapter(adapter);

    mRefId.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (!TextUtils.isEmpty(mRefId.getText()) && !TextUtils.isEmpty(mComment.getText())) {

          SendBtn.setEnabled(true);
          SendBtn.setAlpha(1f);
        } else {
          SendBtn.setEnabled(false);
          SendBtn.setAlpha(0.5f);
        }

      }
    });

    mComment.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (!TextUtils.isEmpty(mComment.getText()) && !TextUtils.isEmpty(mRefId.getText())) {

          SendBtn.setEnabled(true);
          SendBtn.setAlpha(1f);
        } else {
          SendBtn.setEnabled(false);
          SendBtn.setAlpha(0.5f);
        }

      }
    });

    SendBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        // Dismiss keyboard
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        RefId = mRefId.getText().toString().trim();
        Comment = mComment.getText().toString().trim().replaceAll(" ", "%20");
        Rating = (int) mRating.getRating();

        if (validateQuestionnaires(getQuestionnaireResult())) {

          if (connChecker.isAvailable()) {

            submitFeedBackTask();
//                    String requestBody = toJSON(QuestionnaireResult_toJSON(getQuestionnaireResult())).toString();
//                    System.out.println("Request Body = "+requestBody);
          } else {

            Toast.makeText(FeedBack_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }

        } else {

          new Alert_Dialog(FeedBack_Activity.this, "Please answer all questions");
        }

      }
    });

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    switch (id) {
      case android.R.id.home:
        FeedBack_Activity.this.finish();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
