package telnetspdevoloper.nnramobile.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import telnetspdevoloper.nnramobile.R;

public class Confirmation_Activity extends AppCompatActivity {

  private TextView mBookingId;
  private Button mFinishBtn;
  private String RefID;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    RefID = getIntent().getStringExtra("KEY_BookID");
    setContentView(R.layout.activity_confirmation);

    mBookingId = (TextView) findViewById(R.id.book_ref_id);
    mBookingId.setText(RefID);

    mFinishBtn = (Button) findViewById(R.id.btn_finish);
    mFinishBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Confirmation_Activity.this.finish();
      }
    });
  }
}
