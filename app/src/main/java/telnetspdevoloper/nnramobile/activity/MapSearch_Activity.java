package telnetspdevoloper.nnramobile.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.Application.ConnectionChecker;
import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.ConnectivityReceiver;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.adapters.MapSearch_RecyclerAdapter;
import telnetspdevoloper.nnramobile.model.Hospital;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;
import telnetspdevoloper.nnramobile.service.Volley_StringRequest;

public class MapSearch_Activity extends AppCompatActivity implements OnMapReadyCallback,
    LocationListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    ConnectivityReceiver.ConnectivityReceiverListener {

  private GoogleMap myMap;
  GoogleApiClient mGoogleApiClient;
  Boolean mapLoaded = false;

  SearchView searchView = null;
  private SimpleCursorAdapter myAdapter;

  private static final String MYTAG = "MYTAG";

  ProgressBar progress;
  TextView mapMessage;
  Boolean checkGPS = false;
  Boolean checkNetwork = false;
  public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;
  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class
  LocationRequest mLocationRequest;

  LatLng latLng;
  Marker currLocationMarker;

  private ArrayList<Hospital> Hospital_List;

  RecyclerView mRecycler;
  LinearLayoutManager mLayoutManager = null;
  MapSearch_RecyclerAdapter adapter;

  private ConnChecker connChecker = new ConnChecker(MapSearch_Activity.this);

  private String[] strArrData = {"No Suggestions"};

  private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete";
  private static final String OUT_TYPE = "/json";
  private static final String API_KEY = "AIzaSyDBazIiBn2tTmqcSpkH65Xq5doTSuOo22A";

  private String[] ServicesArray;
  private String[] Services_ids;

  public void showSettingsAlert() {
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapSearch_Activity.this);

    alertDialog.setTitle("GPS Not Enabled");

    alertDialog.setMessage("Do you want to turn On GPS");

    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        MapSearch_Activity.this.startActivity(intent);
      }
    });

    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.cancel();

        progress.setVisibility(View.GONE); // dismiss progress dialog
        adapter = null;
        mRecycler.setAdapter(adapter);
        mapMessage.setText("Enable GPS To Locate Diagnostic Center(s)");
        mapMessage.setVisibility(View.VISIBLE); // hide no centers available message
      }
    });

    alertDialog.show();
  }

  private void autocomplete(String search_text) {

    final ArrayList<String> resultList = new ArrayList<>();

    StringBuilder sb = new StringBuilder(PLACES_API_BASE + OUT_TYPE);
    sb.append("?key=" + API_KEY);
    sb.append("&components=country:ng");
    sb.append("&input=" + search_text.replaceAll(" ", "%20"));

    Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.GET, sb.toString(),
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            try {
              // create a JSON object hierarchy from the results
              JSONObject jsonObj = new JSONObject(response);
              System.out.println("The response from google = " + jsonObj.toString());
              JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

              // extract the Place descriptions from the results
              for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).get("description").toString());
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }

            strArrData = resultList.toArray(new String[resultList.size()]);

          }
        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {

      }
    });

    stringRequest.setPriority(Request.Priority.HIGH);
    helper.add(stringRequest);

  }

  private void getLocation(String url) {

    progress.setVisibility(View.VISIBLE); // show progress bar

    Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.GET, url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            JSONObject location;
            Double lng = 0.0;
            Double lat = 0.0;

            try {

              location = new JSONObject(response);

              lng = ((JSONArray) location.get("results")).getJSONObject(0).getJSONObject("geometry")
                  .getJSONObject("location")
                  .getDouble("lng");

              lat = ((JSONArray) location.get("results")).getJSONObject(0).getJSONObject("geometry")
                  .getJSONObject("location")
                  .getDouble("lat");

            } catch (JSONException e) {
              e.printStackTrace();
            }

            Show_Markers(lat, lng);

          }
        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {

        Toast.makeText(MapSearch_Activity.this, "Location not found", Toast.LENGTH_LONG).show();
      }
    });

    stringRequest.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    stringRequest.setRetryPolicy(policy);
    helper.add(stringRequest);
  }

  private void getServicesData(String Medical_Center_Id,
      final String centerName) {  // method call to medical services API

    final ProgressDialog progressDialog = new ProgressDialog(MapSearch_Activity.this);
    progressDialog.setMessage("Retrieving Available Service(s)");
    progressDialog.setCanceledOnTouchOutside(false);
    progressDialog.show();  // display a progress dialog

    JSONObject jobj = new JSONObject();
    try {
      jobj.put("id", Medical_Center_Id);

    } catch (JSONException e) {
      e.printStackTrace();
    }
    //String url = EndPoints.BASE+"getMedicalServices/"+Medical_Center_Id;
    String url = EndPoints.BASE + "getMedicalServices?request=" + jobj;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            progressDialog.dismiss();

            try {

              if (response.length() > 0) {

                ServicesArray = new String[response.length()];
                Services_ids = new String[response.length()];

                for (int i = 0; i < response.length(); i++) {

                  try {
                    String id = response.getJSONObject(i).get("id").toString();
                    String service = response.getJSONObject(i).get("sname").toString();

                    Services_ids[i] = id;
                    ServicesArray[i] = service;

                  } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MapSearch_Activity.this,
                        "Hospital Service(s) Currently Not Available", Toast.LENGTH_LONG).show();
                  }
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(MapSearch_Activity.this);
                builder.setTitle(centerName + " Service(s)");
                builder.setItems(ServicesArray, null);
                builder.setPositiveButton("DONE", null);
                builder.show();
              } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(MapSearch_Activity.this);
                builder.setMessage("Services Currently Not Available");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {

                  }
                });
                builder.show();
              }

            } catch (Exception e) {
              e.printStackTrace();
              Toast.makeText(MapSearch_Activity.this, "Hospital Service(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();
            }

            progressDialog.dismiss();  // dismiss progress dialog
          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
        Toast.makeText(MapSearch_Activity.this, "Failed To Establish Connection To Remote Server",
            Toast.LENGTH_LONG).show();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);
  }

  private void Center_Details_Dialog(String center_name, String center_address, final String phone,
      String email) {

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.center_details_dialog, null);
    ColorGenerator generator = ColorGenerator.MATERIAL; // for generating random colors

    ImageView imageView = (ImageView) v.findViewById(R.id.NameIcon);
    generator = ColorGenerator.MATERIAL; // for generating random colors

    String letter = String.valueOf(center_name.charAt(0));
    TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
    imageView.setImageDrawable(drawable);

    TextView mCenterName = (TextView) v.findViewById(R.id.dialog_center_name_title);
    TextView mCenterAddress = (TextView) v.findViewById(R.id.dialog_center_address);
    TextView mPhoneNO = (TextView) v.findViewById(R.id.dialog_center_phone_no);
    TextView mEmail = (TextView) v.findViewById(R.id.dialog_center_email);

    mCenterName.setText(center_name);
    mCenterAddress.setText(center_address);
    mPhoneNO.setText(phone);
    mEmail.setText(email);

    final AlertDialog.Builder builder = new AlertDialog.Builder(MapSearch_Activity.this);
    builder.setView(v);

    builder.setNegativeButton("Dismiss", null);
    builder.show();

    mPhoneNO.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (phone.length() > 0) {

          try {

            //String dialerNO = "tel:"+phone.replaceAll(" ", "");
            Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(dialIntent);

          } catch (Exception e) {
            e.printStackTrace();
          }

        }
      }
    });

  }

  private void Show_Markers(double mLatitude,
      double mLongitude) { // this method is for showing markers after a search has been done in order for map camera to navigate to the position

    String url = EndPoints.BASE_2 + "NNRA/getListOfCordinates/" + mLatitude + "/" + mLongitude;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                Hospital_List = new ArrayList<>();
                mapMessage.setVisibility(View.GONE); // hide no centers available message

                myMap.clear();

                for (int i = 0; i < response.length(); i++) {

                  try {

                    int centerId = response.getJSONObject(i).getInt("id");
                    String id = response.getJSONObject(i).get("facilityID").toString();
                    String name = response.getJSONObject(i).get("centerName").toString();
                    String address = response.getJSONObject(i).get("address").toString();
                    String longitude = response.getJSONObject(i).get("longitude").toString();
                    String latitude = response.getJSONObject(i).get("latitude").toString();
                    String email = response.getJSONObject(i).get("email").toString();
                    String fullname = response.getJSONObject(i).get("fullAddress").toString();
                    String phoneNo = response.getJSONObject(i).get("phoneNO").toString();

                    Hospital hospital = new Hospital();
                    hospital.setId(id);
                    hospital.setCenterId(centerId);
                    hospital.setHospitalName(name);
                    hospital.setHospitalAddress(address);
                    hospital.setHospitalLatitude(latitude);
                    hospital.setHospitalLongitude(longitude);
                    hospital.setHospitalEmail(email);
                    hospital.setHospitalfullAddress(fullname);
                    hospital.setHospitialPhone(phoneNo);

                    Hospital_List.add(0, hospital);

                  } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                        Toast.LENGTH_LONG).show();

                  }

                }

                // plot center(s) on google map
                for (int i = 0; i < Hospital_List.size(); i++) {

                  // Add Marker to Map
                  MarkerOptions options = new MarkerOptions();
                  options.title(Hospital_List.get(i).getHospitalName());
                  options.snippet(Hospital_List.get(i).getHospitalAddress());
                  //options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
                  options.position(
                      new LatLng(Double.parseDouble(Hospital_List.get(i).getHospitalLatitude()),
                          Double.parseDouble(Hospital_List.get(i).getHospitalLongitude())));
                  Marker currentMarker = myMap.addMarker(options);
                  currentMarker.showInfoWindow();

                }

                //zoom to current position:
                LatLng newPosition = new LatLng(
                    Double.parseDouble(Hospital_List.get(0).getHospitalLatitude()),
                    Double.parseDouble(Hospital_List.get(0).getHospitalLongitude()));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(newPosition)
                    .tilt(40).zoom(15).build();

                myMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

                progress.setVisibility(View.GONE); // dismiss progress dialog
                // setup recycler view and its adapter
                adapter = new MapSearch_RecyclerAdapter(MapSearch_Activity.this, Hospital_List,
                    new MapSearch_RecyclerAdapter.onClickListener() {

                      @Override
                      public void onClick(ArrayList<Hospital> hospitals, final int position) {

                        String[] options = {"View Approved Service(s)", "View Center Details",
                            "Book Appointment"};

                        ListView lvDialog = new ListView(MapSearch_Activity.this);
                        ListAdapter listAdapter = new ArrayAdapter<>(MapSearch_Activity.this,
                            android.R.layout.simple_list_item_1, options);
                        lvDialog.setAdapter(listAdapter);

                        final AlertDialog builder = new AlertDialog.Builder(MapSearch_Activity.this)
                            .create();
                        builder.setView(lvDialog);
                        builder.show();
                        lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                          @Override
                          public void onItemClick(AdapterView<?> adapterView, View view, int i,
                              long l) {

                            if (i == 0) {

                              getServicesData(Hospital_List.get(position).getId(),
                                  Hospital_List.get(position).getHospitalName());

                            } else if (i == 1) {

                              String name = Hospital_List.get(position).getHospitalName();
                              String fulladdress = Hospital_List.get(position)
                                  .getHospitalfullAddress();
                              String phone = Hospital_List.get(position).getHospitialPhone();
                              String email = Hospital_List.get(position).getHospitalEmail();

                              Center_Details_Dialog(name, fulladdress, phone, email);
                            } else {

                              Intent intent = new Intent(MapSearch_Activity.this,
                                  Book_Activity.class);
                              intent.putExtra("KEY_CENTER_ID", Hospital_List.get(position).getId());
                              intent.putExtra("KEY_CENTER_Id",
                                  Hospital_List.get(position).getCenterId());
                              intent.putExtra("Key_Center_Name",
                                  Hospital_List.get(position).getHospitalName());
                              intent.putExtra("Key_Center_Address",
                                  Hospital_List.get(position).getHospitalAddress());
                              intent.putExtra("Key_Center_email",
                                  Hospital_List.get(position).getHospitalEmail());

                              startActivity(intent);

                            }

                            builder.dismiss();
                          }
                        });

                      }
                    });
                mRecycler.setAdapter(adapter);

              } else { // nothing found

                myMap.clear();

                progress.setVisibility(View.GONE); // dismiss progress dialog
                adapter = null;
                mRecycler.setAdapter(adapter);
                mapMessage.setVisibility(View.VISIBLE);
                Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                    Toast.LENGTH_LONG).show();

              }

            } catch (Exception e) {
              e.printStackTrace();
              Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private void ShowMarkers(double mLatitude, double mLongitude) {

    String url = EndPoints.BASE_2 + "NNRA/getListOfCordinates/" + mLatitude + "/" + mLongitude;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                Hospital_List = new ArrayList<>();
                mapMessage.setVisibility(View.GONE); // hide no centers available message

                for (int i = 0; i < response.length(); i++) {

                  try {

                    int centerId = response.getJSONObject(i).getInt("id");
                    String id = response.getJSONObject(i).get("facilityID").toString();
                    String name = response.getJSONObject(i).get("centerName").toString();
                    String address = response.getJSONObject(i).get("address").toString();
                    String longitude = response.getJSONObject(i).get("longitude").toString();
                    String latitude = response.getJSONObject(i).get("latitude").toString();
                    String email = response.getJSONObject(i).get("email").toString();
                    String fullname = response.getJSONObject(i).get("fullAddress").toString();
                    String phoneNo = response.getJSONObject(i).get("phoneNO").toString();

                    Hospital hospital = new Hospital();
                    hospital.setId(id);
                    hospital.setCenterId(centerId);
                    hospital.setHospitalName(name);
                    hospital.setHospitalAddress(address);
                    hospital.setHospitalLatitude(latitude);
                    hospital.setHospitalLongitude(longitude);
                    hospital.setHospitalEmail(email);
                    hospital.setHospitalfullAddress(fullname);
                    hospital.setHospitialPhone(phoneNo);

                    Hospital_List.add(0, hospital);

                  } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                        Toast.LENGTH_LONG).show();

                  }

                }

                // plot center(s) on google map
                for (int i = 0; i < Hospital_List.size(); i++) {

                  // Add Marker to Map
                  MarkerOptions options = new MarkerOptions();
                  options.title(Hospital_List.get(i).getHospitalName());
                  options.snippet(Hospital_List.get(i).getHospitalAddress());
                  //options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
                  options.position(
                      new LatLng(Double.parseDouble(Hospital_List.get(i).getHospitalLatitude()),
                          Double.parseDouble(Hospital_List.get(i).getHospitalLongitude())));
                  Marker currentMarker = myMap.addMarker(options);
                  currentMarker.showInfoWindow();

                }

                progress.setVisibility(View.GONE); // dismiss progress dialog
                // setup recycler view and its adapter
                adapter = new MapSearch_RecyclerAdapter(MapSearch_Activity.this, Hospital_List,
                    new MapSearch_RecyclerAdapter.onClickListener() {

                      @Override
                      public void onClick(ArrayList<Hospital> hospitals, final int position) {

                        String[] options = {"View Approved Service(s)", "View Center Details",
                            "Book Appointment"};

                        ListView lvDialog = new ListView(MapSearch_Activity.this);
                        ListAdapter listAdapter = new ArrayAdapter<>(MapSearch_Activity.this,
                            android.R.layout.simple_list_item_1, options);
                        lvDialog.setAdapter(listAdapter);

                        final AlertDialog builder = new AlertDialog.Builder(MapSearch_Activity.this)
                            .create();
                        builder.setView(lvDialog);
                        builder.show();
                        lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                          @Override
                          public void onItemClick(AdapterView<?> adapterView, View view, int i,
                              long l) {

                            if (i == 0) {

                              getServicesData(Hospital_List.get(position).getId(),
                                  Hospital_List.get(position).getHospitalName());

                            } else if (i == 1) {

                              String name = Hospital_List.get(position).getHospitalName();
                              String fulladdress = Hospital_List.get(position)
                                  .getHospitalfullAddress();
                              String phone = Hospital_List.get(position).getHospitialPhone();
                              String email = Hospital_List.get(position).getHospitalEmail();

                              Center_Details_Dialog(name, fulladdress, phone, email);
                            } else {

                              Intent intent = new Intent(MapSearch_Activity.this,
                                  Book_Activity.class);
                              intent.putExtra("KEY_CENTER_ID", Hospital_List.get(position).getId());
                              intent.putExtra("KEY_CENTER_Id",
                                  Hospital_List.get(position).getCenterId());
                              intent.putExtra("Key_Center_Name",
                                  Hospital_List.get(position).getHospitalName());
                              intent.putExtra("Key_Center_Address",
                                  Hospital_List.get(position).getHospitalAddress());
                              intent.putExtra("Key_Center_email",
                                  Hospital_List.get(position).getHospitalEmail());

                              startActivity(intent);

                            }

                            builder.dismiss();
                          }
                        });

                      }
                    });
                mRecycler.setAdapter(adapter);

              } else { // nothing found

                myMap.clear();

                progress.setVisibility(View.GONE); // dismiss progress dialog
                adapter = null;
                mRecycler.setAdapter(adapter);
                mapMessage.setVisibility(View.VISIBLE); // hide no centers available message

                Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                    Toast.LENGTH_LONG).show();

              }

            } catch (Exception e) {
              e.printStackTrace();
              Toast.makeText(MapSearch_Activity.this, "Center(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private void askPermissionsAndShowMyLocation() {

    // with API >= 23, you have to ask the user for permission to view their location.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      int accessCoarsePermission = ContextCompat.checkSelfPermission(MapSearch_Activity.this,
          android.Manifest.permission.ACCESS_COARSE_LOCATION);
      int accessFinePermission = ContextCompat.checkSelfPermission(MapSearch_Activity.this,
          android.Manifest.permission.ACCESS_FINE_LOCATION);

      if (accessCoarsePermission != PackageManager.PERMISSION_GRANTED ||
          accessFinePermission != PackageManager.PERMISSION_GRANTED) {

        // The permission to ask user
        String[] permissions = new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION};

        // show a dialog asking the user to allow the above permissions.
        ActivityCompat.requestPermissions(MapSearch_Activity.this, permissions,
            REQUEST_ID_ACCESS_COURSE_FINE_LOCATION);

        return;
      }
    }

    // Show current location on Map
    myMap.setMyLocationEnabled(true);
    //showMarkers();
  }

  protected synchronized void buildGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
  }

  private void onMyMapReady(GoogleMap googleMap) {
    // Get Google Map from Fragment
    myMap = googleMap;
    // Set onMapLoadedCallback listner
    myMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
      @Override
      public void onMapLoaded() {
        // Map loaded, Dismiss the dialog

        mapLoaded = true;

        askPermissionsAndShowMyLocation();

        if (ActivityCompat.checkSelfPermission(MapSearch_Activity.this,
            android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(MapSearch_Activity.this,
            android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        if (checkGPS) {

          myMap.setMyLocationEnabled(true);
          buildGoogleApiClient();

          mGoogleApiClient.connect();

        } else {

          showSettingsAlert();
        }

      }
    });
    myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    myMap.getUiSettings().setZoomControlsEnabled(true);

  }

  // Showing the status in Snackbar
  private void showSnack(boolean isConnected) {
    String message = "";
    int color = -0;
//        if (isConnected) {
//           // message = "Good! Connected to Internet";
//            //color = Color.WHITE;
//        } else {
//            message = "Sorry! No Connection To Internet";
//        }

    if (!isConnected) {
      message = "Sorry! No Connection To Internet";

      Snackbar snackbar = Snackbar
          .make(findViewById(R.id.list_hospital_centers), message, Snackbar.LENGTH_LONG);

      View sbView = snackbar.getView();
      //sbView.setBackgroundColor();
      TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
      textView.setTextColor(ContextCompat.getColor(MapSearch_Activity.this, R.color.White));
      snackbar.show();
    }

  }

  // Method to manually check connection status
  private void checkConnection() {
    boolean isConnected = ConnectivityReceiver.isConnected();
    showSnack(isConnected);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_map_search_);

    mRecycler = (RecyclerView) findViewById(R.id.list_hospital_centers);
    progress = (ProgressBar) findViewById(R.id.map_progress);
    mapMessage = (TextView) findViewById(R.id.map_msg);

    mRecycler.setHasFixedSize(true);
    mRecycler.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(MapSearch_Activity.this);
    mRecycler.setLayoutManager(mLayoutManager);

    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.search_map_fragment);

    // set callback listner, on Google Map ready
    mapFragment.getMapAsync(new OnMapReadyCallback() {
      @Override
      public void onMapReady(GoogleMap googleMap) {

        onMyMapReady(googleMap);

        // getting GPS status
        LocationManager locationManager = (LocationManager) MapSearch_Activity.this
            .getSystemService(LOCATION_SERVICE);

        checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

      }
    });

    final String[] from = new String[]{"Result"};
    final int[] to = new int[]{android.R.id.text1};

    // setup SimpleCursorAdapter
    myAdapter = new SimpleCursorAdapter(MapSearch_Activity.this,
        android.R.layout.simple_spinner_dropdown_item, null,
        from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

  }

  @Override
  protected void onStart() {
    super.onStart();

    if (mapLoaded) {

      // getting GPS status
      LocationManager locationManager = (LocationManager) MapSearch_Activity.this
          .getSystemService(LOCATION_SERVICE);

      checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

      if (checkGPS) {
        if (ActivityCompat
            .checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat
            .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        myMap.setMyLocationEnabled(true);
        buildGoogleApiClient();

        mGoogleApiClient.connect();
      } else {

        showSettingsAlert();

      }
    }

  }

  // When you have the request results
  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    switch (requestCode) {
      case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {
        // NOte: If request is cancelled, the result arrays are empty.
        // Permissions granted (read/write)
        if (grantResults.length > 1
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

          Toast.makeText(MapSearch_Activity.this, "Permission granted!", Toast.LENGTH_LONG).show();

        }
        // Cancelled or denied.
        else {
          Toast.makeText(MapSearch_Activity.this, "Permission Denied!", Toast.LENGTH_LONG).show();
        }

        break;
      }
    }
  }

  // Find Location provider is openning
  private String getEnabledLoactionProvider() {

    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    // Criteria to find location provider
    Criteria criteria = new Criteria();

    // Returns the name of the provider that best meets the given criteria.
    // ==> "gps", "network",... etc
    String bestProvider = locationManager.getBestProvider(criteria, true);

    boolean enabled = locationManager.isProviderEnabled(bestProvider);

    if (!enabled) {
      Toast.makeText(MapSearch_Activity.this, "No Location provider enabled!", Toast.LENGTH_LONG)
          .show();
      return null;
    }

    return bestProvider;
  }

  // Call this method only when you have the permissions to view a user's location.
  private void showMyLocation() {
    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    String locationProvider = MapSearch_Activity.this.getEnabledLoactionProvider();

    if (locationProvider == null) {
      return;
    }

    // Millisecond
    final long MIN_TIME_BW_UPDATES = 1000;
    // Met
    final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;

    Location myLocation = null;
    try {
      // This code need permissions (Asked above ***)
      locationManager.requestLocationUpdates(locationProvider, MIN_TIME_BW_UPDATES,
          MIN_DISTANCE_CHANGE_FOR_UPDATES,
          (android.location.LocationListener) MapSearch_Activity.this);

      myLocation = locationManager.getLastKnownLocation(locationProvider);

    } catch (SecurityException e) {
      Toast.makeText(MapSearch_Activity.this, "Show My Location Error: " + e.getMessage(),
          Toast.LENGTH_LONG).show();
      e.printStackTrace();

      return;
    }

    if (myLocation != null) {

      LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
      myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

      CameraPosition cameraPosition = new CameraPosition.Builder()
          .target(latLng)  // sets the center of the map to locaation of user
          .zoom(15)        // sets the zoom
          .bearing(90)     // sets the orientation of the camera to east
          .tilt(40)        // sets the tilt of the camera to 30 degrees
          .build();        // Creates a CameraPosition from the builder
      myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

      // Add Marker to Map
      MarkerOptions options = new MarkerOptions();
      options.title("My Location");
      options.snippet("...");
      options.position(latLng);
      Marker currentMarker = myMap.addMarker(options);
      currentMarker.showInfoWindow();
    } else {
      Toast.makeText(MapSearch_Activity.this, "Location not found!", Toast.LENGTH_LONG).show();
    }
  }

  @Override
  public void onLocationChanged(Location location) {

    //place marker at current position
    //mGoogleMap.clear();
    if (currLocationMarker != null) {
      currLocationMarker.remove();
      currLocationMarker = null;

    }
    latLng = new LatLng(location.getLatitude(), location.getLongitude());
    MarkerOptions markerOptions = new MarkerOptions();
    markerOptions.position(latLng);
    markerOptions.title("Your Location");
    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
    currLocationMarker = myMap.addMarker(markerOptions);

    //zoom to current position:
    CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(latLng)
        .tilt(40).zoom(15).build();

    myMap.animateCamera(CameraUpdateFactory
        .newCameraPosition(cameraPosition));

    //If you only need one location, unregister the listener
    //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    // new getLocations().execute();  // get coordinate location from service

  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    ActivityCompat#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for ActivityCompat#requestPermissions for more details.
      return;
    }
    try {

      LocationManager locationManager = (LocationManager) MapSearch_Activity.this
          .getSystemService(LOCATION_SERVICE);

      // getting GPS status
      checkGPS = locationManager
          .isProviderEnabled(LocationManager.GPS_PROVIDER);

      if (!checkGPS) {

        showSettingsAlert();
      } else {

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
            mGoogleApiClient);
        if (mLastLocation != null) {

          if (currLocationMarker != null) {
            currLocationMarker.remove();
            currLocationMarker = null;

          }
          //place marker at current position
          latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
          MarkerOptions markerOptions = new MarkerOptions();
          markerOptions.position(latLng);
          markerOptions.title("Your Location");
          markerOptions
              .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
          currLocationMarker = myMap.addMarker(markerOptions);

          if (connChecker.isAvailable()) {

            // get coordinate location from service and plot pushpin on google map
            ShowMarkers(mLastLocation.getLatitude(), mLastLocation.getLongitude());

          } else {

            Toast.makeText(MapSearch_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }

        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(300000); // milliseconds
        //mLocationRequest.setFastestInterval(15000); // milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
            (com.google.android.gms.location.LocationListener) MapSearch_Activity.this);

      }
    } catch (Exception e) {

    }

  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void onMapReady(GoogleMap googleMap) {

  }

  @Override
  protected void onResume() {
    super.onResume();

    // register connection status listener
    ConnectionChecker.getInstance().setConnectivityListener(this);
  }

  /**
   * Callback will be triggered when there is change in network connection
   */
  @Override
  public void onNetworkConnectionChanged(boolean isConnected) {
    showSnack(isConnected);
  }

//    // Every time when you press search button on keypad an Activity is recreated which in turn calls this function
//    @Override
//    protected void onNewIntent(Intent intent) {
//
//        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
//            String query = intent.getStringExtra(SearchManager.QUERY);
//            if (searchView != null) {
//                searchView.clearFocus();
//            }
//
//            // User entered text and pressed search button. Perform task ex: fetching data from database and display
//
//        }
//    }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    // add item to action bar
    getMenuInflater().inflate(R.menu.menu_search, menu);

    // Get Search item from action bar and Get Search Service
    MenuItem searchItem = menu.findItem(R.id.search);
    SearchManager searchManager = (SearchManager) MapSearch_Activity.this
        .getSystemService(Context.SEARCH_SERVICE);
    if (searchItem != null) {

      searchView = (SearchView) searchItem.getActionView();
    }

    if (searchView != null) {
      searchView.setSearchableInfo(
          searchManager.getSearchableInfo(MapSearch_Activity.this.getComponentName()));
      searchView.setIconified(true);
      searchView.setSuggestionsAdapter(myAdapter);

      // Getting selected(clicked) items suggested
      searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
        @Override
        public boolean onSuggestionSelect(int position) {
          return true;
        }

        @Override
        public boolean onSuggestionClick(int position) {
          // Add clicked text to search box
          CursorAdapter cursorAdapter = searchView.getSuggestionsAdapter();
          Cursor cursor = cursorAdapter.getCursor();
          cursor.moveToPosition(position);
          searchView.setQuery(cursor.getString(cursor.getColumnIndex("Result")), false);

          // Dismiss keyboard
          InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
          imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

          String address = cursor.getString(cursor.getColumnIndex("Result")).trim();
          if (!address.isEmpty()) {
            // get coordinate of selected location
            String API_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address
                .replaceAll(" ", "%20") + "&key=" + API_KEY + "&sensor=false";

            getLocation(API_URL); // get coordinate of address from google places API

          }

          return true;
        }
      });

      searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

        @Override
        public boolean onQueryTextSubmit(String query) {
          return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {

          autocomplete(newText.trim());

          System.out.println("String array data size = " + strArrData.length);
          // Filter data
          final MatrixCursor mc = new MatrixCursor(new String[]{BaseColumns._ID, "Result"});
          for (int i = 0; i < strArrData.length; i++) {

            if (strArrData[i].toLowerCase().startsWith(newText.toLowerCase())) {
              mc.addRow(new Object[]{i, strArrData[i]});
            }

          }
          myAdapter.changeCursor(mc);
          return false;
        }
      });
    }

    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    switch (id) {
      case android.R.id.home:
        MapSearch_Activity.this.finish();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
