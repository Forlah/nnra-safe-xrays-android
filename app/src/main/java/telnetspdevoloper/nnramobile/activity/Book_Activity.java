package telnetspdevoloper.nnramobile.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.Alert_Dialog;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.Utilities.Progressdialog;
import telnetspdevoloper.nnramobile.adapters.MyServicesAdapter;
import telnetspdevoloper.nnramobile.model.Booking;
import telnetspdevoloper.nnramobile.model.HospitalService;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;
import telnetspdevoloper.nnramobile.service.Volley_StringRequest;

public class Book_Activity extends AppCompatActivity {

  private ImageView imageView;
  ColorGenerator generator = ColorGenerator.MATERIAL; // for generating random colors
  private String Name_Title, Medical_Center_Id, Medical_Center_Address, Medical_Center_Email, PatientName, Email, Phone, Comment, AppointmentDate;
  private int Center_ID = -2;
  private TextView mCenter_Name_Text, NameErrorText, EmailErrorText, ServicesErrorText, PhoneErrorText;
  private TextView mSelectServices;
  RecyclerView recyclerView;
  MyServicesAdapter adapter;
  private static EditText mAppointmentDate;
  private EditText mPatientName, mEmail, mPhone, mComment;
  private static final java.lang.String PICKER = "datePicker";
  private static Date newdate;
  private Button BookBtn;
  private static TextView ErrorText;

  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class
  private ArrayList<HospitalService> selected_services_list = new ArrayList<>();
  private String[] ServicesArray;
  private String[] Services_ids;
  private ConnChecker connChecker = new ConnChecker(Book_Activity.this);
  private boolean EmailErrorvisible = false;

  private static String Date_text_formatter(Date date) {  // date string formatter

    SimpleDateFormat y = new SimpleDateFormat("yyyy");
    SimpleDateFormat m = new SimpleDateFormat("MM");
    SimpleDateFormat d = new SimpleDateFormat("dd");
    // SimpleDateFormat D = new SimpleDateFormat("EEEE , MMMM dd, yyyy");
    SimpleDateFormat D = new SimpleDateFormat("dd - MM - yyyy");

    String x;

    //  x =D.format(date)+"-"+ d.format(date)+"-"+m.format(date)+"-"+y.format(date);
    x = D.format(date);

    return x;
  }

  private JSONObject toJSON(Booking booking) {  // convert to json

    JSONObject jsonObject = new JSONObject();
    JSONObject jsonObject_medical = new JSONObject();
    JSONArray services_array = new JSONArray();
    try {
      jsonObject.put("customerName", booking.getCustomerName());
      jsonObject.put("email", booking.getEmail());
      jsonObject.put("mobileNO", booking.getPhone());
      jsonObject.put("appointmentDate", booking.getAppointmentDate());
      jsonObject.put("description", booking.getDescription());

      jsonObject_medical.put("id", booking.getMedicalCenter().getCenterId());
      jsonObject_medical.put("email", Medical_Center_Email);
      jsonObject_medical.put("address", Medical_Center_Address);
      jsonObject_medical.put("centerName", Name_Title);

      jsonObject.put("medicalCenters", jsonObject_medical);

      for (int i = 0; i < booking.getServices().size(); i++) {
        JSONObject object = new JSONObject();
        object.put("id", Integer.parseInt(booking.getServices().get(i).getId()));
        services_array.put(object);
      }

      jsonObject.put("service", services_array);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return jsonObject;
  }

  private static boolean isValidEmail(String email) {
    boolean isValid = false;

    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
    CharSequence inputStr = email;

    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(inputStr);
    if (matcher.matches()) {
      isValid = true;
    }
    return isValid;
  }

  private Boolean isExistingInList(String item_id) {
    Boolean isAvailable = false;

    for (int i = 0; i < selected_services_list.size(); i++) {

      if (item_id.equals(selected_services_list.get(i).getId())) {

        isAvailable = true;
        break;
      }
    }

    return isAvailable;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Medical_Center_Id = getIntent()
        .getStringExtra("KEY_CENTER_ID"); // get center Id text from intent

    Center_ID = getIntent().getIntExtra("KEY_CENTER_Id", -1);

    Name_Title = getIntent().getStringExtra("Key_Center_Name"); // get center name text from intent

    Medical_Center_Address = getIntent()
        .getStringExtra("Key_Center_Address"); // get center address text from intent

    Medical_Center_Email = getIntent()
        .getStringExtra("Key_Center_email"); // get center email text from intent

    //Medical_Center_Id = "MDC110";

    setContentView(R.layout.activity_book_);

    generator = ColorGenerator.MATERIAL; // for generating random colors

    getServicesData();  // webservice call to get available services rendered by a medical center

    recyclerView = (RecyclerView) findViewById(R.id.required_services_recycler);
    recyclerView.setHasFixedSize(false);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setLayoutManager(new LinearLayoutManager(Book_Activity.this));

    imageView = (ImageView) findViewById(R.id.NameIcon);
    mCenter_Name_Text = (TextView) findViewById(R.id.center_name_txt);
    mCenter_Name_Text.setText(Name_Title);

    String letter = String.valueOf(Name_Title.charAt(0));
    TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
    imageView.setImageDrawable(drawable);

    ErrorText = (TextView) findViewById(R.id.err_msg);
    NameErrorText = (TextView) findViewById(R.id.name_err_msg);
    EmailErrorText = (TextView) findViewById(R.id.email_err_msg);
    PhoneErrorText = (TextView) findViewById(R.id.phone_err_msg);
    ServicesErrorText = (TextView) findViewById(R.id.required_services_err_msg);
    BookBtn = (Button) findViewById(R.id.book_Btn);
    mEmail = (EditText) findViewById(R.id.email);
    mPhone = (EditText) findViewById(R.id.phone);
    mComment = (EditText) findViewById(R.id.complaint_desc);
    mPatientName = (EditText) findViewById(R.id.name);
    mAppointmentDate = (EditText) findViewById(R.id.appointment_date);
    mSelectServices = (TextView) findViewById(R.id.services_txt_btn);

    mSelectServices.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (ServicesArray == null) {
          // pass by
          AlertDialog.Builder builder = new AlertDialog.Builder(Book_Activity.this);
          builder.setMessage("Services Currently Not Available");
          builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
          });
          builder.show();
        } else {

          AlertDialog.Builder builder = new AlertDialog.Builder(Book_Activity.this);
          builder.setTitle("Choose Service");
          builder.setItems(ServicesArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

              HospitalService obj = new HospitalService();
              obj.setId(Services_ids[i]);
              obj.setServiceTitle(ServicesArray[i]);

              if (isExistingInList(Services_ids[i])
                  == false) { // if the item to be added has not been added already

                if (selected_services_list.size() > 0) {

                  selected_services_list.add(obj);
                  final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                      .getLayoutManager();
                  adapter.notifyItemInserted(linearLayoutManager.getItemCount());

                } else {

                  selected_services_list.add(obj);
                  recyclerView.setVisibility(View.VISIBLE);
                  adapter = new MyServicesAdapter(Book_Activity.this, selected_services_list);
                  recyclerView.setAdapter(adapter);

                }

              }

              if (selected_services_list.size() > 0) {
                ServicesErrorText.setVisibility(View.INVISIBLE);
              }

            }
          });
          builder.show();
        }

      }
    });

    mAppointmentDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        FragmentManager fm = getFragmentManager();
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(fm, PICKER);
      }
    });

    mPatientName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (TextUtils.isEmpty(mPatientName.getText())) {

          NameErrorText.setText("This Field Is Required");
          NameErrorText.setVisibility(View.VISIBLE);
        } else {
          NameErrorText.setVisibility(View.GONE);

        }
      }
    });

    mPhone.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (TextUtils.isEmpty(mPhone.getText())) {

          PhoneErrorText.setVisibility(View.GONE);
        } else if (mPhone.getText().toString().length() < 11) {
          PhoneErrorText.setText("Incomplete Phone Number");
          PhoneErrorText.setVisibility(View.VISIBLE);
        } else if (EmailErrorvisible) {
          EmailErrorText.setVisibility(View.GONE);
        } else {
          PhoneErrorText.setVisibility(View.GONE);
        }
      }
    });

    mEmail.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (TextUtils.isEmpty(mEmail.getText())) {

          EmailErrorText.setVisibility(View.GONE);
        } else if (!isValidEmail(mEmail.getText().toString())) {
          EmailErrorText.setText("Invalid Email Address");
          EmailErrorText.setVisibility(View.VISIBLE);

          EmailErrorvisible = true;
        } else {
          EmailErrorText.setVisibility(View.GONE);
        }

      }
    });

    BookBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

//                // Dismiss keyboard
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        PatientName = mPatientName.getText().toString().trim().replaceAll(" ", "%20");
        Email = mEmail.getText().toString().trim();
        Comment = mComment.getText().toString().trim().replaceAll(" ", "%20");
        AppointmentDate = mAppointmentDate.getText().toString().trim();
        Phone = mPhone.getText().toString().trim();

        if (TextUtils.isEmpty(mPatientName.getText()) && TextUtils.isEmpty(mEmail.getText())
            && TextUtils.isEmpty(mPhone.getText())
            && TextUtils.isEmpty(mComment.getText()) && TextUtils
            .isEmpty(mAppointmentDate.getText())) {

          NameErrorText.setText("This Field Is Required");
          NameErrorText.setVisibility(View.VISIBLE);

          EmailErrorText.setText("Phone number or email is required");
          EmailErrorText.setVisibility(View.VISIBLE);

          mAppointmentDate.setError("This field is required");
          mComment.setError("This Field Is Required");
        } else if (TextUtils.isEmpty(mPatientName.getText())) {

          NameErrorText.setText("This Field Is Required");
          NameErrorText.setVisibility(View.VISIBLE);
        }

//

        else if (Phone.length() < 11) {

          PhoneErrorText.setText("11 Digit Mobile Number Required");
          PhoneErrorText.setVisibility(View.VISIBLE);

        } else if (TextUtils.isEmpty(mAppointmentDate.getText())) {
          mAppointmentDate.setError("This Field Is Required");
        } else if (TextUtils.isEmpty(mComment.getText())) {
          mComment.setError("This Field Is Required");
        } else if (selected_services_list.size() > 0 == false) {

          ServicesErrorText.setVisibility(View.VISIBLE);

          Toast.makeText(Book_Activity.this, "Please Select at least One(1) Service",
              Toast.LENGTH_LONG).show();
        } else {

          Booking booking = new Booking();
          booking.setCustomerName(PatientName);
          booking.setEmail(Email);
          booking.setPhone(Phone);
          booking.setAppointmentDate(AppointmentDate);
          booking.setDescription(Comment);
          booking.getMedicalCenter().setCenterId(Center_ID);
          booking.setServices(selected_services_list);

          if (connChecker.isAvailable()) {

            if (TextUtils.isEmpty(mEmail.getText())) {

              BookAppointment_Service(booking); // API call to make booking

            } else {

              if (isValidEmail(Email)) {

                BookAppointment_Service(booking); // API call to make booking

              } else {

                EmailErrorText.setText("This Field Is Required");
                EmailErrorText.setVisibility(View.VISIBLE);
              }

            }

          } else {

            Toast.makeText(Book_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
                .show();
          }

        }

      }
    });

  }

  private void getServicesData() {  // method call to medical services API

    final ProgressDialog progressDialog = new ProgressDialog(Book_Activity.this);
    progressDialog.setMessage("Retrieving Available Service(s)");
    progressDialog.setCanceledOnTouchOutside(false);
    progressDialog.show();  // display a progress dialog

    JSONObject jobj = new JSONObject();
    try {
      jobj.put("id", Medical_Center_Id);

    } catch (JSONException e) {
      e.printStackTrace();
    }
    //String url = EndPoints.BASE+"getMedicalServices/"+Medical_Center_Id;
    String url = EndPoints.BASE + "getMedicalServices?request=" + jobj;

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, url, null,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {

            try {

              if (response.length() > 0) {

                ServicesArray = new String[response.length()];
                Services_ids = new String[response.length()];

                for (int i = 0; i < response.length(); i++) {

                  try {
                    String id = response.getJSONObject(i).get("id").toString();
                    String service = response.getJSONObject(i).get("sname").toString();

                    Services_ids[i] = id;
                    ServicesArray[i] = service;

                  } catch (JSONException e) {
                    e.printStackTrace();

                    Toast
                        .makeText(Book_Activity.this, "Hospital Service(s) Currently Not Available",
                            Toast.LENGTH_LONG).show();

                  }

                }
              }

            } catch (Exception e) {
              e.printStackTrace();

              Toast.makeText(Book_Activity.this, "Hospital Service(s) Currently Not Available",
                  Toast.LENGTH_LONG).show();

            }

            progressDialog.dismiss();  // dismiss progress dialog
          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
        Toast.makeText(Book_Activity.this, "Failed To Establish Connection To Remote Server",
            Toast.LENGTH_LONG).show();
      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);
  }

  private void BookAppointment_Service(Booking booking) {

    final Progressdialog pd = new Progressdialog();
    pd.show_progress_no_title(Book_Activity.this, "Please Wait...");

    final String requestBody = toJSON(booking).toString();

    // System.out.println("request Body = "+requestBody);

    String url = EndPoints.BASE + "makeBooking";

    final Volley_StringRequest request = new Volley_StringRequest(Request.Method.POST, url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

            //System.out.println("make Booking response = "+ response);

            pd.dismissProgress();

            if (response.trim().contains("NNRA")) {

              Intent i = new Intent(Book_Activity.this, Confirmation_Activity.class);
              i.putExtra("KEY_BookID", response.trim());
              startActivity(i);

              finish();
            } else if (response.trim().startsWith("0")) {
              new Alert_Dialog(Book_Activity.this,
                  "Sorry, We Were Unable To Book Your Appointment.. ");

            } else {

              new Alert_Dialog(Book_Activity.this, response.toString());
            }

          }

        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        Toast.makeText(Book_Activity.this, "Failed To Establish Connection To Remote Server",
            Toast.LENGTH_LONG).show();
        pd.dismissProgress();
      }

    }) {
      @Override
      public String getBodyContentType() {
        return "application/json; charset=utf-8";
      }

      @Override
      public byte[] getBody() throws AuthFailureError {

        try {

          return requestBody == null ? null : requestBody.getBytes("utf-8");

        } catch (UnsupportedEncodingException uee) {
          VolleyLog
              .wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody,
                  "utf-8");
          return null;
        }
      }

      @Override
      protected Response<String> parseNetworkResponse(
          NetworkResponse response) { // can get response code here
        return super.parseNetworkResponse(response);
      }

    };

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout, 0, 1f);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();
    switch (id) {

      case android.R.id.home:
        Book_Activity.this.finish();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  public static class DatePickerFragment extends DialogFragment implements
      DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
      // use the current date as the default date
      final Calendar c = Calendar.getInstance();
      int year = c.get(Calendar.YEAR);
      int month = c.get(Calendar.MONTH);
      int day = c.get(Calendar.DAY_OF_MONTH);

      return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear,
        int dayOfMonth) {

      newdate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();

      int current_date = Integer.parseInt(new SimpleDateFormat("dd").format(new Date()));

      if (newdate.compareTo(new Date()) < 0) {

        if (!TextUtils.isEmpty(mAppointmentDate.getText())) {
          mAppointmentDate.setText("");
        }
        ErrorText.setVisibility(View.VISIBLE);
      } else if (dayOfMonth == current_date) {

        if (!TextUtils.isEmpty(mAppointmentDate.getText())) {
          mAppointmentDate.setText("");
        }
        ErrorText.setVisibility(View.VISIBLE);
      } else {

        mAppointmentDate.setText(Date_text_formatter(newdate));
        ErrorText.setVisibility(View.GONE);

      }

    }
  }

}
