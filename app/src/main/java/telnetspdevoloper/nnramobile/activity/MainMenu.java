package telnetspdevoloper.nnramobile.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import telnetspdevoloper.nnramobile.R;

public class MainMenu extends AppCompatActivity {

  private RelativeLayout mSearch, mVerify, mFeedback, mFAQs, mNNRADirect, mAbout;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main_menu);

    mSearch = (RelativeLayout) findViewById(R.id.rel_search);
    mVerify = (RelativeLayout) findViewById(R.id.rel_booking);
    mFeedback = (RelativeLayout) findViewById(R.id.rel_feedback);
    mFAQs = (RelativeLayout) findViewById(R.id.rel_faqs);
    // mNNRADirect = (RelativeLayout)findViewById(R.id.rel_nnra_direct);
    mAbout = (RelativeLayout) findViewById(R.id.rel_about);

    mSearch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Intent i = new Intent(MainMenu.this, MapSearch_Activity.class);
        startActivity(i);
      }
    });

    mVerify.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Intent intent = new Intent(MainMenu.this, Verify_Activity.class);
        startActivity(intent);
      }
    });

    mFeedback.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Intent intent = new Intent(MainMenu.this, FeedBack_Activity.class);
        startActivity(intent);
      }
    });

    mFAQs.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Intent intent = new Intent(MainMenu.this, FAQs_Activity.class);
        startActivity(intent);
      }
    });

    mAbout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Intent intent = new Intent(MainMenu.this, About.class);
        startActivity(intent);
      }
    });

  }
}
