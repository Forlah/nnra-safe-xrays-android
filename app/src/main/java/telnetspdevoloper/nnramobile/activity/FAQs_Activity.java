package telnetspdevoloper.nnramobile.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.Alert_Dialog;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.Utilities.Progressdialog;
import telnetspdevoloper.nnramobile.adapters.ExpandableList_Adapter;
import telnetspdevoloper.nnramobile.model.Groups;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;

public class FAQs_Activity extends AppCompatActivity {

  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class

  // private ArrayList<Groups> ExpandableListData;
  private ExpandableListView mExpandable;
  private ExpandableList_Adapter Adapter;
  private static final String FAQ_URL = EndPoints.BASE + "getFAQ";
  private ConnChecker connChecker = new ConnChecker(FAQs_Activity.this);

  FAQsList_Adapter faqsAdapter;
  private ListView mlistview;

  private ArrayList<Groups> setMainGroups() {

    ArrayList<Groups> Groups_ItemList = new ArrayList<>();

    for (int i = 0; i < 20; i++) {

      Groups group = new Groups();
      group.setQuestion("Q" + i
          + " What Is The Full Meaning Of NNRA What Is The Full Meaning Of NNRA What Is The Full Meaning Of NNRA");
      String ans = "Ans " + i
          + " Nigeria Nuclear Regulation Authority Nigeria Nuclear Regulation Authority Nigeria Nuclear Regulation Authority Nigeria Nuclear Regulation Authority";
      ArrayList<String> child = new ArrayList<>();
      child.add(ans);
      group.setAnswer(child);

      Groups_ItemList.add(group);
    }

    return Groups_ItemList;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_faqs_);

//        mExpandable =  (ExpandableListView)findViewById(R.id.exp_list);
//        mExpandable.setGroupIndicator(null); // remove dropdown arrow indicator
//        mExpandable.setChildIndicator(null); // remove horizontal line divider for children

    mlistview = (ListView) findViewById(R.id.faqs_list);

    if (connChecker.isAvailable()) {

      // get coordinate location from service and plot pushpin on google map
      getFAQsTask();

    } else {

      Toast.makeText(FAQs_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG).show();
    }

    //volleyCacheRequest(FAQ_URL);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {

      case android.R.id.home:
        FAQs_Activity.this.finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void volleyCacheRequest(String url) {
    Cache cache = VolleyAppl.getInstance().getRequestQueue().getCache();
    Cache.Entry reqEntry = cache.get(url);
    if (reqEntry != null) {

      //Handle the Data here.

      String data = null;
      JSONArray jsonArray = null;
      try {
        data = new String(reqEntry.data, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }

      try {

        jsonArray = new JSONArray(data);
        ArrayList<Groups> Groups_ItemList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {

          String mQuestion = jsonArray.getJSONObject(i).get("question").toString();
          String mAnswer = jsonArray.getJSONObject(i).get("answer").toString();

          Groups group = new Groups();
          group.setQuestion(mQuestion);
          ArrayList<String> child = new ArrayList<>();
          child.add(mAnswer);
          group.setAnswer(child);

          Groups_ItemList.add(group);

        }

        Adapter = new ExpandableList_Adapter(FAQs_Activity.this, Groups_ItemList);
        mExpandable.setAdapter(Adapter);
        mExpandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
          @Override
          public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
              int childPosition, long id) {
            return false;
          }
        });
        mExpandable.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
          @Override
          public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition,
              long id) {
            return false;
          }
        });
      } catch (JSONException e) {
        e.printStackTrace();
      }

      refreshFAQsTask(jsonArray); // check for changes and update view
    } else {

      //Request Not present in cache, launch a network request instead.
      getFAQsTask();
    }

  }

  private void getFAQsTask() {

    final Progressdialog pd = new Progressdialog();
    pd.show_progress_no_title(FAQs_Activity.this, "Please Wait..");
    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, FAQ_URL,
        null, new Response.Listener<JSONArray>() {
      @Override
      public void onResponse(JSONArray response) {

        try {

          ArrayList<Groups> Groups_ItemList = new ArrayList<>();
          for (int i = 0; i < response.length(); i++) {

            String mQuestion = response.getJSONObject(i).get("question").toString();
            String mAnswer = response.getJSONObject(i).get("answer").toString();

            Groups group = new Groups();
            group.setQuestion(mQuestion);
            group.setmAnswer(mAnswer);

            ArrayList<String> child = new ArrayList<>();
            child.add(mAnswer);
            group.setAnswer(child);

            Groups_ItemList.add(group);

          }

//                    Adapter = new ExpandableList_Adapter(FAQs_Activity.this,Groups_ItemList);
//                    mExpandable.setAdapter(Adapter);
//                    mExpandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//                        @Override
//                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                            return false;
//                        }
//                    });
//                    mExpandable.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//                        @Override
//                        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                            return false;
//                        }
//                    });

          faqsAdapter = new FAQsList_Adapter(Groups_ItemList);
          mlistview.setAdapter(faqsAdapter);

          pd.dismissProgress();

        } catch (Exception e) {
          e.printStackTrace();
          pd.dismissProgress();

          Toast.makeText(FAQs_Activity.this, "Service Not Available", Toast.LENGTH_LONG).show();

        }

      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {

        pd.dismissProgress();

        error.printStackTrace();

        new Alert_Dialog(FAQs_Activity.this, "Connection Lost",
            "Please Ensure You Have Internet Connection.");

      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout, 0, 1.0f);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  private void refreshFAQsTask(final JSONArray cacheValue) {

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET, FAQ_URL,
        null, new Response.Listener<JSONArray>() {
      @Override
      public void onResponse(JSONArray response) {

        try {

          if (response.length() < cacheValue.length() || response.length() > cacheValue.length()) {

            ArrayList<Groups> Groups_ItemList = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {

              String mQuestion = response.getJSONObject(i).get("question").toString();
              String mAnswer = response.getJSONObject(i).get("answer").toString();

              Groups group = new Groups();
              group.setQuestion(mQuestion);
              group.setmAnswer(mAnswer);

              ArrayList<String> child = new ArrayList<>();
              child.add(mAnswer);
              group.setAnswer(child);

              Groups_ItemList.add(group);

            }

            Adapter = new ExpandableList_Adapter(FAQs_Activity.this, Groups_ItemList);
            mExpandable.setAdapter(Adapter);

            Adapter.notifyDataSetChanged(); // update listview content

          } else {
            // no changes available
            // Toast.makeText(FAQs_Activity.this, "There were no changes", Toast.LENGTH_LONG).show();

          }

        } catch (JSONException e) {

        }

      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Toast.makeText(FAQs_Activity.this, "Connection Problem", Toast.LENGTH_LONG).show();

      }
    });

    request.setPriority(Request.Priority.NORMAL);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);
  }

//    public void volleyInvalidateCache(String url){
//        AppSingleton.getInstance(getApplicationContext()).getRequestQueue().getCache().invalidate(url, true);
//    }
//
//    public void volleyDeleteCache(String url){
//        AppSingleton.getInstance(getApplicationContext()).getRequestQueue().getCache().remove(url);
//    }
//
//    public void volleyClearCache(){
//        AppSingleton.getInstance(getApplicationContext()).getRequestQueue().getCache().clear();
//    }

  private class FAQsList_Adapter extends ArrayAdapter<Groups> {

    ArrayList<Groups> Items;
    // Context mContext;

    public FAQsList_Adapter(ArrayList<Groups> items) {
      super(FAQs_Activity.this, 0, items);
      Items = items;
      //  mContext = c;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      //if there is no view available , inflate one
      if (convertView == null) {
        convertView = getLayoutInflater().inflate(R.layout.faqs_row, null);
      }

      Groups groups = Items.get(position);

      TextView mFullname = (TextView) convertView.findViewById(R.id.faqs_question_txt);
      mFullname.setText(groups.getQuestion());

      WebView webView = (WebView) convertView.findViewById(R.id.faqs_answer_txt);
      // mText.setText(grps.get(childPosition));
      webView.loadData(groups.getmAnswer(), "text/html", "utf-8");
      final WebSettings settings = webView.getSettings();
      //Resources res = mContext.getResources();
      settings.setDefaultFontSize(Integer.parseInt("19")); // text size

      return convertView;
    }
  }

}
