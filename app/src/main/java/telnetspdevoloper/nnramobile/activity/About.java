package telnetspdevoloper.nnramobile.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.widget.TextView;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.Constants;

public class About extends AppCompatActivity {

  private TextView mAboutText;
  Spanned htmlAsSpanned;

  @Override
  @SuppressWarnings("deprecation")
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
    mAboutText = (TextView) findViewById(R.id.about_us_text);

    // get our html content
    String htmlAsString = getString(R.string.about_);      // used by WebView
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

      htmlAsSpanned = Html.fromHtml(htmlAsString, Html.FROM_HTML_MODE_LEGACY);

    } else {
      htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView
    }

    mAboutText.setText(htmlAsSpanned);
    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    mAboutText.setTypeface(font);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    switch (id) {
      case android.R.id.home:
        About.this.finish();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
