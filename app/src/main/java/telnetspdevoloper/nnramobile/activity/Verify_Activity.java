package telnetspdevoloper.nnramobile.activity;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collection;

import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.Utilities.Alert_Dialog;
import telnetspdevoloper.nnramobile.Utilities.ConnChecker;
import telnetspdevoloper.nnramobile.Utilities.Constants;
import telnetspdevoloper.nnramobile.Utilities.EndPoints;
import telnetspdevoloper.nnramobile.Utilities.Progressdialog;
import telnetspdevoloper.nnramobile.adapters.ExpandableList_Adapter;
import telnetspdevoloper.nnramobile.adapters.Verify_RecyclerAdapter;
import telnetspdevoloper.nnramobile.model.Groups;
import telnetspdevoloper.nnramobile.model.Verify;
import telnetspdevoloper.nnramobile.service.Volley_JsonArray_Request;

public class Verify_Activity extends AppCompatActivity {

  private EditText mCenterName;
  private Button VerifyBtn;
  private String CenterName;
  private RecyclerView mRecyclerView;
  private ConnChecker connChecker = new ConnChecker(Verify_Activity.this);
  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class
  private String safeURl;
  LinearLayoutManager mLayoutManager = null;
  RelativeLayout mCenterRelative;
  LinearLayout mCustomerLinear;
  private ArrayList<Verify> listOfVerifiedCenters = new ArrayList<>();

  private TextView NotAvailableTxt, Center_Text, Status;
  Verify_RecyclerAdapter adapter;

  private Boolean verifyUserCenter(ArrayList<Verify> items, String name) {
    Boolean isValid = false;

    for (Verify verify : items) {

      if (verify.getCenterName().toLowerCase().equals(name.toLowerCase())) {

        isValid = true;

        break;
      }
    }

    return isValid;
  }

  private void getVerifiedCenters(final String centerName) {

    String GET_Verified_URL =
        EndPoints.BASE_2 + "NNRA/getMedicalFilter?centerName=" + centerName.replaceAll(" ", "%20");
    final Progressdialog pd = new Progressdialog();

    pd.show_progress_no_title(Verify_Activity.this, "Please Wait..");

    Volley_JsonArray_Request request = new Volley_JsonArray_Request(Request.Method.GET,
        GET_Verified_URL, null, new Response.Listener<JSONArray>() {

      @Override
      public void onResponse(JSONArray response) {

        pd.dismissProgress();

        if (response.length() > 0) {

          listOfVerifiedCenters.clear(); // clear list

          for (int i = 0; i < response.length(); i++) {

            try {

              String name = response.getJSONObject(i).get("centerName").toString();
              String address = response.getJSONObject(i).get("address").toString();
              int status = response.getJSONObject(i).getInt("isActive");

              Verify obj = new Verify();
              obj.setCenterName(name);
              obj.setCenterAddress(address);
              obj.setStatus(String.valueOf(status));

              listOfVerifiedCenters.add(obj);

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

          adapter = new Verify_RecyclerAdapter(Verify_Activity.this, listOfVerifiedCenters);
          mRecyclerView.setAdapter(adapter);

          if (verifyUserCenter(listOfVerifiedCenters, centerName)) {

            Center_Text.setText(CenterName);
            Status.setText("SAFE");
            Status.setTextColor(ContextCompat.getColor(Verify_Activity.this, R.color.Green));

          } else {

            Center_Text.setText(CenterName);
            Status.setText("NOT SAFE");
            Status.setTextColor(ContextCompat.getColor(Verify_Activity.this, R.color.Red));
          }

          mCenterRelative.setVisibility(View.VISIBLE);
          mCustomerLinear.setVisibility(View.VISIBLE);
          NotAvailableTxt.setVisibility(View.GONE);

        } else {

          NotAvailableTxt.setVisibility(View.VISIBLE);
        }

      }
    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {

        pd.dismissProgress();

        error.printStackTrace();

        new Alert_Dialog(Verify_Activity.this, "Connection Lost",
            "Please Ensure You Have Internet Connection.");

      }
    });

    request.setPriority(Request.Priority.HIGH);
    RetryPolicy policy = new DefaultRetryPolicy(Constants.SocketTimeout,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    request.setRetryPolicy(policy);
    helper.add(request);

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_);

    mCenterName = (EditText) findViewById(R.id.center_name);
    NotAvailableTxt = (TextView) findViewById(R.id.available_txt);
    mCustomerLinear = (LinearLayout) findViewById(R.id.linear_1);
    mCenterRelative = (RelativeLayout) findViewById(R.id.rel_2);
    Status = (TextView) findViewById(R.id.state);
    Center_Text = (TextView) findViewById(R.id.user_txt);

    VerifyBtn = (Button) findViewById(R.id.verify_btn);

    mRecyclerView = (RecyclerView) findViewById(R.id.verified_list);

    mRecyclerView.setHasFixedSize(true);

    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(Verify_Activity.this);
    mRecyclerView.setLayoutManager(mLayoutManager);

    VerifyBtn.setEnabled(false);
    VerifyBtn.setAlpha(0.5f);

    mCenterName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        if (mCenterName.getText().toString().trim().equals("")
            || mCenterName.getText().length() < 0) {
          VerifyBtn.setEnabled(false);
          VerifyBtn.setAlpha(0.5f);

        } else {
          VerifyBtn.setEnabled(true);
          VerifyBtn.setAlpha(1f);
        }

      }
    });

    VerifyBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        CenterName = mCenterName.getText().toString();

        if (connChecker.isAvailable()) {

          getVerifiedCenters(CenterName);

        } else {

          Toast.makeText(Verify_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    switch (id) {
      case android.R.id.home:
        Verify_Activity.this.finish();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
