package telnetspdevoloper.nnramobile.Application;

import android.app.Application;

import telnetspdevoloper.nnramobile.Utilities.ConnectivityReceiver;

/**
 * Created by sp_developer on 2/17/17.
 */
public class ConnectionChecker extends Application {

  private static ConnectionChecker mInstance;

  @Override
  public void onCreate() {
    super.onCreate();

    mInstance = this;
  }

  public static synchronized ConnectionChecker getInstance() {
    return mInstance;
  }

  public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
    ConnectivityReceiver.connectivityReceiverListener = listener;
  }

}
