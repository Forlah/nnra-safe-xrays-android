package telnetspdevoloper.nnramobile.Application;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import telnetspdevoloper.nnramobile.Utilities.ConnectivityReceiver;

/**
 * Created by sp_developer on 2/17/17.
 */
public class VolleyAppl extends ConnectionChecker {

  private RequestQueue mRequestQueue;
  private static VolleyAppl mInstance;
  public static final String TAG = VolleyAppl.class.getName();

  @Override
  public void onCreate() {
    super.onCreate();

    mInstance = this;
    mRequestQueue = Volley.newRequestQueue(getApplicationContext());
  }

  public static synchronized VolleyAppl getInstance() {

    return mInstance;
  }

  public RequestQueue getRequestQueue() {

    return mRequestQueue;
  }

  public <T> void add(Request<T> req) {
    req.setTag(TAG);
    getRequestQueue().add(req);
  }

  public void cancel() {
    mRequestQueue.cancelAll(TAG);
  }
}
