package telnetspdevoloper.nnramobile.Utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sp_developer on 2/22/17.
 */
public class ConnChecker {

  private Context context;

  public ConnChecker(Context c) {

    context = c;
  }

  public boolean isAvailable() {
    ConnectivityManager conMgr = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
    if (networkInfo != null && networkInfo.isConnected()) {
      return true;
    } else {
      return false;
    }
  }

}
