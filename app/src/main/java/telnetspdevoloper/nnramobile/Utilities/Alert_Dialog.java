package telnetspdevoloper.nnramobile.Utilities;

import android.content.Context;
import android.support.v7.app.AlertDialog;

/**
 * Created by sp_developer on 2/21/17.
 */
public class Alert_Dialog {

  Context mContext;
  android.support.v7.app.AlertDialog.Builder builder;

  // Empty constructor for accessing class
  public Alert_Dialog() {

  }

  public Alert_Dialog(Context context, String msg) {
    mContext = context;
    builder = new android.support.v7.app.AlertDialog.Builder(mContext);
    builder.setTitle("ATTENTION");
    builder.setMessage(msg);
    builder.setPositiveButton("OK", null);
    builder.show();
  }

  public Alert_Dialog(Context context, String title, String msg) {

    mContext = context;
    builder = new android.support.v7.app.AlertDialog.Builder(mContext);
    builder.setTitle(title);
    builder.setMessage(msg);
    builder.setPositiveButton("OK", null);
    builder.show();
  }
}
