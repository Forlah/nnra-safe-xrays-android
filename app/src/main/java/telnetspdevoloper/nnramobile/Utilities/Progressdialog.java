package telnetspdevoloper.nnramobile.Utilities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

import telnetspdevoloper.nnramobile.R;

/**
 * Created by sp_developer on 2/21/17.
 */
public class Progressdialog {

  private Context context;
  private String message;
  private ProgressDialog pd;

  public Progressdialog() {

  }

  public void showProgress() {
    pd = new ProgressDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
    pd.setMessage(message);
    pd.setTitle("Processing");
    pd.setCanceledOnTouchOutside(false);
    //pd.setProgressStyle(Alert_Dialog.THEME_HOLO_DARK);
    pd.setProgressStyle(R.style.AppTheme);
    pd.show();
  }

  public void showProgress_Title() {
    pd = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
    pd.setMessage(message);
    pd.setTitle("Processing");
    pd.setCanceledOnTouchOutside(false);
    // pd.setProgressStyle(Alert_Dialog.THEME_HOLO_DARK);
    pd.show();
  }

  public void set_showProgreses(Context c, String title, String msg) {
    pd = new ProgressDialog(c, AlertDialog.THEME_HOLO_LIGHT);
    pd.setMessage(msg);
    pd.setTitle(title);
    pd.setCanceledOnTouchOutside(false);
    pd.show();
  }

  public void show_progress_no_title(Context c, String msg) {
    pd = new ProgressDialog(c, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
    pd.setMessage(msg);
    pd.setProgressStyle(R.style.AppTheme);
    pd.setCanceledOnTouchOutside(false);
    pd.show();
  }

  public void dismissProgress() {
    pd.dismiss();
  }

}
