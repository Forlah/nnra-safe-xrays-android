package telnetspdevoloper.nnramobile.Utilities;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telnetspdevoloper.nnramobile.Application.VolleyAppl;
import telnetspdevoloper.nnramobile.service.Volley_StringRequest;

/**
 * Created by sp_developer on 2/23/17.
 */
public class ServiceConnection {

  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class

  private String String_response = null;

  public String getStringResponse(String url, String method) {

    if (method.equals("GET")) {

      Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.GET, url,
          new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

              String_response = response;

            }
          }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
      });

      stringRequest.setPriority(Request.Priority.HIGH);
      helper.add(stringRequest);

    }

    return String_response;

  }
}
