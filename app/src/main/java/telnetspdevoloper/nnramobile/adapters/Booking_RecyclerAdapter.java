package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Hospital;

/**
 * Created by sp_developer on 2/17/17.
 */
public class Booking_RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context context;
  private ArrayList<Hospital> Items;
  ColorGenerator generator = ColorGenerator.MATERIAL; // for generating random colors

  private bookOnClickListener listener;

  public Booking_RecyclerAdapter(Context context, ArrayList<Hospital> items,
      bookOnClickListener listener) {
    this.context = context;
    this.Items = items;

    this.listener = listener;
  }

  public interface bookOnClickListener {

    void onClick(int position);
  }

  public class BookingViewHolder extends RecyclerView.ViewHolder {

    TextView Hospital_Name_txt;
    TextView Hospital_Address_txt;
    TextView Book_view_btn;
    ImageView imageLetter;

    View v;

    public BookingViewHolder(View itemView) {
      super(itemView);

      v = itemView;
      Hospital_Name_txt = (TextView) itemView.findViewById(R.id.center_name);
      Hospital_Address_txt = (TextView) itemView.findViewById(R.id.center_address);
      Book_view_btn = (TextView) itemView.findViewById(R.id.book_btn);
      imageLetter = (ImageView) itemView.findViewById(R.id.hospital_letter);

    }

  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.bookings_row, parent, false);

    return new BookingViewHolder(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

    if (holder instanceof BookingViewHolder) {

      final BookingViewHolder bookingViewHolder = (BookingViewHolder) holder;

      String letter = String.valueOf(Items.get(position).getHospitalName().charAt(0));

      TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
      bookingViewHolder.imageLetter.setImageDrawable(drawable);
      bookingViewHolder.Hospital_Name_txt.setText(Items.get(position).getHospitalName());
      bookingViewHolder.Hospital_Address_txt.setText(Items.get(position).getHospitalAddress());
      bookingViewHolder.Book_view_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

          listener.onClick(position); // configure listener
        }
      });

    }

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }

}
