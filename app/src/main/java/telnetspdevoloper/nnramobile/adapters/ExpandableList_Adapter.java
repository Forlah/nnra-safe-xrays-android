package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Groups;

/**
 * Created by sp_developer on 3/2/17.
 */
public class ExpandableList_Adapter extends BaseExpandableListAdapter {

  Context mContext;
  ArrayList<Groups> mGroups;

  public ExpandableList_Adapter(Context context, ArrayList<Groups> groups) {
    mContext = context;
    mGroups = groups;
  }

  @SuppressWarnings("deprecation")
  private Spanned toHtmlSpannedText(String html_txt) {
    Spanned htmlAsSpanned;
    //String htmlAsString = "<![CDATA[ "+html_txt+ " ]]>";
    String htmlAsString = html_txt;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

      htmlAsSpanned = Html.fromHtml(htmlAsString, Html.FROM_HTML_MODE_LEGACY);

    } else {
      htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView
    }

    return htmlAsSpanned;
  }

  @Override
  public int getGroupCount() {
    return mGroups.size();
  }

  @Override
  public int getChildrenCount(int groupPosition) {
//
    return mGroups.get(groupPosition).getAnswer().size();
  }

  @Override
  public Object getGroup(int groupPosition) {
    return mGroups.get(groupPosition);
  }

  @Override
  public Object getChild(int groupPosition, int childPosition) {
    return mGroups.get(groupPosition).getAnswer();
  }

  @Override
  public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override
  public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  @Override
  public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    Groups grps = (Groups) getGroup(groupPosition);
    if (convertView == null) {
      LayoutInflater inf = (LayoutInflater) mContext
          .getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
      convertView = inf.inflate(R.layout.faq_parent_row, null);
    }

    TextView mText = (TextView) convertView.findViewById(R.id.faqs_question_txt);
    ImageView imageView = (ImageView) convertView.findViewById(R.id.faq_expand_arrow);
    mText.setText(grps.getQuestion());
    if (isExpanded) {
      imageView.setImageResource(R.mipmap.angle_up);
      imageView.setAlpha(0.5f);
    } else if (!isExpanded) {
      imageView.setImageResource(R.mipmap.angle_down);
      imageView.setAlpha(1f);

    }

    return convertView;
  }

  @Override
  public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
      View convertView, ViewGroup parent) {

    ArrayList<String> grps = (ArrayList<String>) getChild(groupPosition, childPosition);
    if (convertView == null) {
      LayoutInflater layoutInflater = (LayoutInflater) mContext
          .getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate(R.layout.faq_child_row, null);
    }

//        TextView mText = (TextView)convertView.findViewById(R.id.faqs_answer_txt);
    WebView webView = (WebView) convertView.findViewById(R.id.faqs_answer_txt);
    // mText.setText(grps.get(childPosition));
    webView.loadData(grps.get(childPosition), "text/html", "utf-8");
    final WebSettings settings = webView.getSettings();
    Resources res = mContext.getResources();
    settings.setDefaultFontSize(Integer.parseInt("19")); // text size

    return convertView;
  }

  @Override
  public boolean isChildSelectable(int groupPosition, int childPosition) {
    return true;
  }

}
