package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Verify;

/**
 * Created by sp_developer on 4/27/17.
 */
public class Verify_RecyclerAdapter extends
    RecyclerView.Adapter<Verify_RecyclerAdapter.DataObjectHolder> {

  private ArrayList<Verify> Items;
  private Context context;

  public Verify_RecyclerAdapter(Context context, ArrayList<Verify> items) {
    Items = items;
    this.context = context;
  }

  public static class DataObjectHolder extends RecyclerView.ViewHolder {

    private TextView name, address, status;

    public View mView = null;

    public DataObjectHolder(View itemView) {
      super(itemView);

      mView = itemView;
      name = (TextView) itemView.findViewById(R.id.name);

      status = (TextView) itemView.findViewById(R.id.status);

      address = (TextView) itemView.findViewById(R.id.address);

    }

  }

  @Override
  public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.verify_row, parent, false);

    DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

    return dataObjectHolder;

  }

  @Override
  public void onBindViewHolder(DataObjectHolder holder, int position) {

    Verify result = Items.get(position);

    holder.name.setText(result.getCenterName());
    holder.address.setText(result.getCenterAddress());
    String status_txt = result.getStatus();

    if (status_txt.startsWith("1")) {
      holder.status.setTextColor(ContextCompat.getColor(context, R.color.Green));
      holder.status.setText("SAFE");

    } else {
      holder.status.setTextColor(ContextCompat.getColor(context, R.color.Red));
      holder.status.setText("NOT SAFE");

    }

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }

}
