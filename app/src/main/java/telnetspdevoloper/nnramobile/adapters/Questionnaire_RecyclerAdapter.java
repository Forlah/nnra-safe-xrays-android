package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Hospital;
import telnetspdevoloper.nnramobile.model.Questions;

/**
 * Created by sp_developer on 3/20/17.
 */
public class Questionnaire_RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  ArrayList<Questions> Items;
  Context context;

  public SparseBooleanArray mCheckStates_Yes;
  public SparseBooleanArray mCheckStates_No;
  public SparseBooleanArray CheckboxResultValue;

  public SparseBooleanArray getmCheckResultState() {
    return mCheckResultState;
  }

  public void setmCheckResultState(SparseBooleanArray mCheckResultState) {
    this.mCheckResultState = mCheckResultState;
  }

  public SparseBooleanArray mCheckResultState;

//    public void setChecked(int position, boolean isChecked, CompoundButton view){
//        // mCheckStates.put(position, isChecked);
//        mCheckStates_Yes.put((Integer)view.getTag(),isChecked);
//        mCheckStates_No.put((Integer)view.getTag(), isChecked);
//
//        setmCheckStates_No(mCheckStates_No);
//        setmCheckStates_Yes(mCheckStates_Yes);
//    }

  public void setCheckedYES(int position, boolean isChecked, CompoundButton view) {

    mCheckStates_Yes.put((Integer) view.getTag(), isChecked);

    setmCheckStates_Yes(mCheckStates_Yes);

  }

  public void setCheckedNO(int position, boolean isChecked, CompoundButton view) {

    mCheckStates_No.put((Integer) view.getTag(), isChecked);
    setmCheckStates_No(mCheckStates_No);

  }

  public void setChecked(int position, boolean isChecked, CompoundButton view) {

    CheckboxResultValue.put((Integer) view.getTag(), isChecked);
    setmCheckResultState(CheckboxResultValue);

  }

  public SparseBooleanArray getChecked() {
    return CheckboxResultValue;
  }

  public SparseBooleanArray getmCheckStates_Yes() {
    return mCheckStates_Yes;
  }

  public void setmCheckStates_Yes(SparseBooleanArray mCheckStates_Yes) {
    this.mCheckStates_Yes = mCheckStates_Yes;
  }

  public SparseBooleanArray getmCheckStates_No() {
    return mCheckStates_No;
  }

  public void setmCheckStates_No(SparseBooleanArray mCheckStates_No) {
    this.mCheckStates_No = mCheckStates_No;
  }

  public Questionnaire_RecyclerAdapter(Context context, ArrayList<Questions> Items) {
    this.Items = Items;
    mCheckStates_No = new SparseBooleanArray(Items.size());
    mCheckStates_Yes = new SparseBooleanArray(Items.size());
    CheckboxResultValue = new SparseBooleanArray(Items.size());

    this.context = context;

  }

  public static class QuestionnaireViewHolder extends RecyclerView.ViewHolder {

    View v;
    TextView Fullname_txt;
    CheckBox mNoCheckbox;
    CheckBox mYesCheckbox;

    public QuestionnaireViewHolder(View itemView) {
      super(itemView);

      v = itemView;

      Fullname_txt = (TextView) itemView.findViewById(R.id.fulltext);
      mNoCheckbox = (CheckBox) itemView.findViewById(R.id.no_checkbox);
      mYesCheckbox = (CheckBox) itemView.findViewById(R.id.yes_checkbox);

    }

  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.questionaire_row, parent, false);

    return new QuestionnaireViewHolder(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

    if (holder instanceof QuestionnaireViewHolder) {

      final QuestionnaireViewHolder questionnaireViewHolder = (QuestionnaireViewHolder) holder;
      Questions ques = Items.get(position);

      questionnaireViewHolder.Fullname_txt.setText(ques.getQuestion_Text());

      questionnaireViewHolder.mYesCheckbox.setTag(position);
      questionnaireViewHolder.mNoCheckbox.setTag(position);

      questionnaireViewHolder.mYesCheckbox.setChecked(mCheckStates_Yes.get(position, false));
      questionnaireViewHolder.mNoCheckbox.setChecked(mCheckStates_No.get(position, false));

      questionnaireViewHolder.mYesCheckbox
          .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

              if (isChecked == true) {

                setChecked(position, true, buttonView);
                setCheckedYES(position, true, buttonView);

                if (questionnaireViewHolder.mNoCheckbox.isChecked()) {

                  questionnaireViewHolder.mNoCheckbox.setChecked(false);

                  setCheckedNO(position, false, buttonView);
                  setChecked(position, false, buttonView);

                }
              } else {
                setChecked(position, false, buttonView);
                setCheckedYES(position, false, buttonView);
              }

            }

          });

      questionnaireViewHolder.mNoCheckbox
          .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

              if (isChecked == true) {

                setChecked(position, true, buttonView);
                setCheckedNO(position, true, buttonView);

                if (questionnaireViewHolder.mYesCheckbox.isChecked()) {

                  questionnaireViewHolder.mYesCheckbox.setChecked(false);

                  setChecked(position, false, buttonView);

                  setCheckedYES(position, false, buttonView);

                }

              } else {
                setChecked(position, false, buttonView);
                setCheckedNO(position, false, buttonView);
              }

            }

          });
    }

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }

}
