package telnetspdevoloper.nnramobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Questions;

/**
 * Created by sp_developer on 3/20/17.
 */
public class Questionnaire_ListAdaper extends ArrayAdapter<Questions> {

  ArrayList<Questions> Items;
  Context context;
  LayoutInflater getInflater;
  Questions question;

  public InfoHolder holder;
  public SparseBooleanArray mCheckStates;

  public Questionnaire_ListAdaper(Context context, ArrayList<Questions> Items) {
    super(context, 0, Items);
    this.Items = Items;
    mCheckStates = new SparseBooleanArray(Items.size());
    this.context = context;

  }

  public SparseBooleanArray getmCheckStates() {
    return mCheckStates;
  }

  public void setmCheckStates(SparseBooleanArray mCheckStates) {
    this.mCheckStates = mCheckStates;
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {

    View row = convertView;

    holder = null;

    if (row == null) {

      getInflater = ((Activity) context).getLayoutInflater();

      row = getInflater.inflate(R.layout.questionaire_row, parent, false);

      holder = new InfoHolder();

      holder.Fullname_txt = (TextView) row.findViewById(R.id.fulltext);
      holder.mNoCheckbox = (CheckBox) row.findViewById(R.id.no_checkbox);
      holder.mYesCheckbox = (CheckBox) row.findViewById(R.id.yes_checkbox);

      row.setTag(holder);

    } else {

      holder = (InfoHolder) row.getTag();
    }

    question = Items.get(position);

    holder.Fullname_txt.setText(question.getQuestion_Text());
    holder.mYesCheckbox.setTag(position);
    holder.mNoCheckbox.setTag(position);

    holder.mYesCheckbox.setChecked(mCheckStates.get(position, false));
    holder.mNoCheckbox.setChecked(mCheckStates.get(position, false));

    holder.mYesCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        setChecked(position, isChecked, buttonView);

        if (isChecked) {
          Toast.makeText(context, "Yes button checked", Toast.LENGTH_LONG).show();

        } else {
          Toast.makeText(context, "Yes button un-checked", Toast.LENGTH_LONG).show();

        }

        // mSelectionTextview.setText("" + SelectedCount() + " of" + " " + Items.size());

      }

    });

    holder.mNoCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        setChecked(position, isChecked, buttonView);

        // mSelectionTextview.setText("" + SelectedCount() + " of" + " " + Items.size());

        if (isChecked) {
          Toast.makeText(context, "No button checked", Toast.LENGTH_LONG).show();

        } else {
          Toast.makeText(context, "No button un-checked", Toast.LENGTH_LONG).show();

        }

      }

    });

    return row;

  }

  public void setChecked(int position, boolean isChecked, CompoundButton view) {
    // mCheckStates.put(position, isChecked);
    mCheckStates.put((Integer) view.getTag(), isChecked);
    setmCheckStates(mCheckStates);
  }

  static class InfoHolder {

    TextView Fullname_txt;
    CheckBox mYesCheckbox, mNoCheckbox;
  }

  public int SelectedCount() {

    int n = 0;

    for (int i = 0; i < Items.size(); i++) {

      if (getmCheckStates().get(i) == true) {
        n++;
      }

    }

    return n;

  }

}
