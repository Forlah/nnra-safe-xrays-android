package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Hospital;

/**
 * Created by sp_developer on 2/16/17.
 */

public class MapSearch_RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context context;
  private ArrayList<Hospital> Items;
  ColorGenerator generator = ColorGenerator.MATERIAL; // for generating random colors

  private onClickListener listener;

  public MapSearch_RecyclerAdapter(Context context, ArrayList<Hospital> items,
      onClickListener listener) {
    this.context = context;
    this.Items = items;

    this.listener = listener;
  }

  public interface onClickListener {

    void onClick(ArrayList<Hospital> hospitals, int position);
  }

  public class MapSearchViewHolder extends RecyclerView.ViewHolder {

    TextView Hospital_Name_txt;
    TextView Hospital_Address_txt;
    ImageView imageLetter;

    //TextView readmore;
    View v;

    public MapSearchViewHolder(View itemView) {
      super(itemView);

      v = itemView;
      Hospital_Name_txt = (TextView) itemView.findViewById(R.id.center_name);
      Hospital_Address_txt = (TextView) itemView.findViewById(R.id.center_address);
      imageLetter = (ImageView) itemView.findViewById(R.id.item_letter);

    }

    public void Binder(final ArrayList<Hospital> hospitals, final onClickListener listener,
        final int position) {
      v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          listener.onClick(hospitals, position);
        }
      });
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.map_search_row, parent, false);

    return new MapSearchViewHolder(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

    if (holder instanceof MapSearchViewHolder) {

      final MapSearchViewHolder mapSearchViewHolder = (MapSearchViewHolder) holder;

      String letter = String.valueOf(Items.get(position).getHospitalName().charAt(0));

      TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
      mapSearchViewHolder.imageLetter.setImageDrawable(drawable);
      mapSearchViewHolder.Hospital_Name_txt.setText(Items.get(position).getHospitalName());
      mapSearchViewHolder.Hospital_Address_txt.setText(Items.get(position).getHospitalAddress());

      mapSearchViewHolder.Binder(Items, listener, position); // configure listener

    }

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }

}
