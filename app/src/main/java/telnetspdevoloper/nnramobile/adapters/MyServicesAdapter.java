package telnetspdevoloper.nnramobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import telnetspdevoloper.nnramobile.R;
import telnetspdevoloper.nnramobile.model.Hospital;
import telnetspdevoloper.nnramobile.model.HospitalService;

/**
 * Created by sp_developer on 11/2/16.
 */
public class MyServicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  Context context;
  LayoutInflater inflater;
  static ArrayList<HospitalService> Item;

  public MyServicesAdapter(Context context, ArrayList<HospitalService> Item) {
    this.context = context;
    inflater = LayoutInflater.from(context);
    this.Item = Item;
  }

  public class myServiceViewHolder extends RecyclerView.ViewHolder {

    View view;
    TextView service_name;
    ImageButton remove_image_btn;

    public myServiceViewHolder(View itemView) {
      super(itemView);

      view = itemView;
      service_name = (TextView) itemView.findViewById(R.id.service_name_txt);
      remove_image_btn = (ImageButton) itemView.findViewById(R.id.remove_hospital_service);
      remove_image_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          int pos = getAdapterPosition();

          try {

            Item.remove(pos);
            notifyItemRemoved(pos);

          } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
          }
        }
      });
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(this.context)
        .inflate(R.layout.hospital_services_row_item, parent, false);
    return new myServiceViewHolder(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    if (holder instanceof myServiceViewHolder) {

      myServiceViewHolder myHolder = (myServiceViewHolder) holder;

      myHolder.service_name.setText(Item.get(position).getServiceTitle());
    }

  }

  @Override
  public int getItemCount() {
    return Item.size();
  }
}

